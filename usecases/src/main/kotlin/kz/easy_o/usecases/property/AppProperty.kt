package kz.easy_o.usecases.property

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "app")
open class AppProperty {
    lateinit var profile: String
}