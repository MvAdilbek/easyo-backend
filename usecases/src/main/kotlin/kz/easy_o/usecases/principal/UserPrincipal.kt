package kz.easy_o.usecases.principal

import kz.easy_o.core.entity.SessionEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.oauth2.core.user.OAuth2User
import java.util.*
import kotlin.collections.HashMap

class UserPrincipal(
        val sessionEntity: SessionEntity
) : OAuth2User, UserDetails {

    companion object {
        fun create(sessionEntity: SessionEntity): UserPrincipal {
            return UserPrincipal(
                    sessionEntity = sessionEntity
            )
        }
    }

    override fun getName(): String = sessionEntity.id?.toString() ?: ""

    override fun getAuthorities(): Collection<GrantedAuthority> = Collections.emptyList()

    override fun getAttributes(): Map<String, Any> = HashMap()

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = ""

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = ""

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true
}