package kz.easy_o.usecases.extension

import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils

fun Errors.rejectIfNull(field: String, errorCode: String) {
    ValidationUtils.rejectIfEmptyOrWhitespace(this, field, errorCode)
}