package kz.easy_o.usecases.module.order_line

import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class OrderLineByIdUseCase(
        private val orderLineRepository: OrderLineRepository
) : UseCase<Long, OrderLineEntity> {
    override fun execute(request: Long): OrderLineEntity {
        return orderLineRepository.findById(request)
                ?: throw  NotFoundException("No OrderLine for id: $request")
    }

    interface OrderLineRepository {
        fun findById(id: Long): OrderLineEntity?
    }
}