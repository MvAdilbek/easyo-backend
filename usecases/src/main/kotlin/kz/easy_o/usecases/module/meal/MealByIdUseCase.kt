package kz.easy_o.usecases.module.meal

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class MealByIdUseCase(
        private val mealRepository: MealRepository
) : UseCase<Long, MealEntity> {
    override fun execute(request: Long): MealEntity {
        return mealRepository.findById(request)
                ?: throw NotFoundException("No Meal for id: $request")
    }

    interface MealRepository {
        fun findById(id: Long): MealEntity?
    }
}