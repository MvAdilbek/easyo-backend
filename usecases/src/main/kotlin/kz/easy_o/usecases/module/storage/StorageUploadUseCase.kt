package kz.easy_o.usecases.module.storage

import kz.easy_o.core.StorageFileConfig
import kz.easy_o.core.parameter.StorageUploadParam
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.service.GoogleStorageService

class StorageUploadUseCase(
        private val googleStorageService: GoogleStorageService
) : UseCase<StorageUploadParam, StorageFileConfig> {

    override fun execute(request: StorageUploadParam): StorageFileConfig {
        val upload =
                googleStorageService.upload(request.filename,
                                            request.byteArray)
        return StorageFileConfig(path = upload.mediaLink)
    }
}