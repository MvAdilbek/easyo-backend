package kz.easy_o.usecases.module.table

import kz.easy_o.core.Page
import kz.easy_o.core.entity.TableEntity
import kz.easy_o.core.parameter.TableParam
import kz.easy_o.usecases.common.UseCase

class TablePageUseCase(
        private val tableRepository: TableRepository
) : UseCase<TableParam, Page<TableEntity>> {
    override fun execute(request: TableParam): Page<TableEntity> {
        return tableRepository.findPageByParam(request)
    }

    interface TableRepository {
        fun findPageByParam(tableParam: TableParam): Page<TableEntity>
    }
}