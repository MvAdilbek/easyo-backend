package kz.easy_o.usecases.module.resource

import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.validator.`object`.ResourceEntityValidator

class ResourceSaveUseCase(
        private val resourceEntityValidator: ResourceEntityValidator,
        private val resourceRepository: ResourceRepository
) : UseCase<ResourceEntity, ResourceEntity> {
    override fun execute(request: ResourceEntity): ResourceEntity {
        resourceEntityValidator.checkEntity(request)

        request.id = resourceRepository.save(request)

        return request
    }

    interface ResourceRepository {
        fun save(resourceEntity: ResourceEntity): Long
    }
}