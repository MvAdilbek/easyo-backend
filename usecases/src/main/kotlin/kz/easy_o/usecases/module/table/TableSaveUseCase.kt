package kz.easy_o.usecases.module.table

import kz.easy_o.core.entity.TableEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.validator.`object`.TableEntityValidator

class TableSaveUseCase(
        private val tableEntityValidator: TableEntityValidator,
        private val tableRepository: TableRepository
) : UseCase<TableEntity, TableEntity> {
    override fun execute(request: TableEntity): TableEntity {
        tableEntityValidator.checkEntity(request)

        request.id = tableRepository.save(request)

        return request
    }

    interface TableRepository {
        fun save(tableEntity: TableEntity): Long
    }
}