package kz.easy_o.usecases.module.bill

import kz.easy_o.core.Page
import kz.easy_o.core.entity.BillEntity
import kz.easy_o.core.parameter.BillParam
import kz.easy_o.usecases.common.UseCase

class BillPageUseCase(
        private val billRepository: BillRepository
) : UseCase<BillParam, Page<BillEntity>> {
    override fun execute(request: BillParam): Page<BillEntity> {
        return billRepository.findPageByParam(request)
    }

    interface BillRepository {
        fun findPageByParam(billParam: BillParam): Page<BillEntity>
    }
}