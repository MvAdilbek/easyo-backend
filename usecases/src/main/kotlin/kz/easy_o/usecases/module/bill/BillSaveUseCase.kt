package kz.easy_o.usecases.module.bill

import kz.easy_o.core.entity.BillEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.validator.`object`.BillEntityValidator

class BillSaveUseCase(
        private val billEntityValidator: BillEntityValidator,
        private val billRepository: BillRepository
) : UseCase<BillEntity, BillEntity> {
    override fun execute(request: BillEntity): BillEntity {
        billEntityValidator.checkEntity(request)
        return billRepository.save(request)
    }

    interface BillRepository {
        fun save(billEntity: BillEntity): BillEntity
    }
}