package kz.easy_o.usecases.module.session

import kz.easy_o.core.parameter.SessionSocketRegisterParam
import kz.easy_o.usecases.common.UseCase

class SessionSocketRegisterUseCase(
        private val sessionRepository: SessionRepository
) : UseCase<SessionSocketRegisterParam, Unit> {
    override fun execute(request: SessionSocketRegisterParam) {
        sessionRepository.updateSocketSessionId(request.sessionId, request.socketSessionId)
    }

    interface SessionRepository {
        fun updateSocketSessionId(sessionId: Long, socketSessionId: String)
    }
}