package kz.easy_o.usecases.module.session

import kz.easy_o.core.constant.Constant
import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.core.parameter.session.SessionSaveParam
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.common.extension.sha256
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.service.GenerateTextService
import kz.easy_o.usecases.validator.`object`.SessionSaveParamValidator
import org.springframework.security.core.userdetails.UsernameNotFoundException
import java.time.LocalDateTime

class SessionSaveUseCase(
        private val sessionSaveParamValidator: SessionSaveParamValidator,
        private val sessionRepository: SessionRepository,
        private val userRepository: UserRepository,
        private val generateTextService: GenerateTextService
) : UseCase<SessionSaveParam, SessionEntity> {

    private val logger = logger<SessionSaveUseCase>()

    override fun execute(request: SessionSaveParam): SessionEntity {
        logger.info("Execute with request: $request")
        sessionSaveParamValidator.checkEntity(request)

        val userEntity = userRepository.findByEmailAndPassword(email = request.login!!,
                                                               password = request.password!!.sha256())
                ?: throw UsernameNotFoundException("No email or password find for user")

        val token = generateTextService.getRandomToken(Constant.TOKEN_SIZE)

        val sessionEntity = SessionEntity(id = null,
                                          token = token,
                                          entryTime = LocalDateTime.now(),
                                          userEntity = userEntity)

        val sessionId = sessionRepository.save(sessionEntity)
        logger.info("Save SessionEntity with id: $sessionId")

        return sessionEntity.also { it.id = sessionId }
    }

    interface SessionRepository {
        fun save(sessionEntity: SessionEntity): Long
        fun deleteByUserId(userId: Long)
    }

    interface UserRepository {
        fun findByEmailAndPassword(email: String, password: String): UserEntity?
    }
}