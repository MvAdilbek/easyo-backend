package kz.easy_o.usecases.module.menu

import kz.easy_o.core.Page
import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.core.parameter.MenuParam
import kz.easy_o.core.parameter.UserParam
import kz.easy_o.usecases.common.UseCase

class MenuPageUseCase(
        private val menuRepository: MenuRepository
) : UseCase<MenuParam, Page<MenuEntity>> {
    override fun execute(request: MenuParam): Page<MenuEntity> {
        return menuRepository.findPageByParam(request)
    }

    interface MenuRepository {
        fun findPageByParam(menuParam: MenuParam): Page<MenuEntity>
    }
}