package kz.easy_o.usecases.module.user

import kz.easy_o.core.entity.UserEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class UserByIdUseCase(
        private val userRepository: UserRepository
) : UseCase<Long, UserEntity> {
    override fun execute(request: Long): UserEntity {
        return userRepository.findById(request)
                ?: throw NotFoundException("No User for id: $request")
    }

    interface UserRepository {
        fun findById(id: Long): UserEntity?
    }
}