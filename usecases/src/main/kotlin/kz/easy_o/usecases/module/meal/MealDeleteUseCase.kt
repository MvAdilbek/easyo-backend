package kz.easy_o.usecases.module.meal

import kz.easy_o.usecases.common.UseCase

class MealDeleteUseCase(
        private val mealRepository: MealRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        mealRepository.deleteById(request)
    }

    interface MealRepository {
        fun deleteById(id: Long)
    }
}