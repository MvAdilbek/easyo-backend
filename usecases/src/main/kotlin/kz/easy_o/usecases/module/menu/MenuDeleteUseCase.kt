package kz.easy_o.usecases.module.menu

import kz.easy_o.usecases.common.UseCase

class MenuDeleteUseCase(
        private val menuRepository: MenuRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        menuRepository.deleteById(request)
    }

    interface MenuRepository {
        fun deleteById(id: Long)
    }
}