package kz.easy_o.usecases.module.order

import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class OrderByIdUseCase(
        private val orderRepository: OrderRepository
) : UseCase<Long, OrderEntity> {
    override fun execute(request: Long): OrderEntity {
        return orderRepository.findById(request)
                ?: throw  NotFoundException("No Order for id: $request")
    }

    interface OrderRepository {
        fun findById(id: Long): OrderEntity?
    }
}