package kz.easy_o.usecases.module.resource

import kz.easy_o.usecases.common.UseCase

class ResourceDeleteUseCase(
        private val resourceRepository: ResourceRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        resourceRepository.deleteById(request)
    }

    interface ResourceRepository {
        fun deleteById(id: Long)
    }
}