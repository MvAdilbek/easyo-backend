package kz.easy_o.usecases.module.table

import kz.easy_o.usecases.common.UseCase

class TableDeleteUseCase(
        private val tableRepository: TableRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        tableRepository.deleteById(request)
    }

    interface TableRepository {
        fun deleteById(id: Long)
    }
}