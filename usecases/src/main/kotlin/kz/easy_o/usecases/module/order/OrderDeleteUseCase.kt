package kz.easy_o.usecases.module.order

import kz.easy_o.usecases.common.UseCase

class OrderDeleteUseCase(
        private val orderRepository: OrderRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        orderRepository.deleteById(request)
    }

    interface OrderRepository {
        fun deleteById(id: Long)
    }
}