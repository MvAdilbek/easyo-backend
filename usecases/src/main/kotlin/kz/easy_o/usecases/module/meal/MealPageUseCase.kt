package kz.easy_o.usecases.module.meal

import kz.easy_o.core.Page
import kz.easy_o.core.entity.MealEntity
import kz.easy_o.core.parameter.MealParam
import kz.easy_o.usecases.common.UseCase

class MealPageUseCase(
        private val mealRepository: MealRepository
) : UseCase<MealParam, Page<MealEntity>> {
    override fun execute(request: MealParam): Page<MealEntity> {
        return mealRepository.findPageByParam(request)
    }

    interface MealRepository {
        fun findPageByParam(mealParam: MealParam): Page<MealEntity>
    }
}