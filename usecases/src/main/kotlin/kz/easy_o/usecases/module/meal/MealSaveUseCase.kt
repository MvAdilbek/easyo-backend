package kz.easy_o.usecases.module.meal

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.validator.`object`.MealEntityValidator

class MealSaveUseCase(
        private val mealEntityValidator: MealEntityValidator,
        private val mealRepository: MealRepository
) : UseCase<MealEntity, MealEntity> {
    override fun execute(request: MealEntity): MealEntity {
        mealEntityValidator.checkEntity(request)
        return mealRepository.save(request)
    }

    interface MealRepository {
        fun save(mealEntity: MealEntity): MealEntity
    }
}