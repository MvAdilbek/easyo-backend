package kz.easy_o.usecases.module.order_line

import kz.easy_o.core.Page
import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.core.parameter.OrderLineParam
import kz.easy_o.usecases.common.UseCase

class OrderLinePageUseCase(
        private val orderLineRepository: OrderLineRepository
) : UseCase<OrderLineParam, Page<OrderLineEntity>> {
    override fun execute(request: OrderLineParam): Page<OrderLineEntity> {
        return orderLineRepository.findPageByParam(request)
    }

    interface OrderLineRepository {
        fun findPageByParam(orderLineParam: OrderLineParam): Page<OrderLineEntity>
    }
}