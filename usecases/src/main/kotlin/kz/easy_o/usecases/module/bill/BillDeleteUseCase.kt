package kz.easy_o.usecases.module.bill

import kz.easy_o.usecases.common.UseCase

class BillDeleteUseCase(
        private val billRepository: BillRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        billRepository.deleteById(request)
    }

    interface BillRepository {
        fun deleteById(id: Long)
    }
}