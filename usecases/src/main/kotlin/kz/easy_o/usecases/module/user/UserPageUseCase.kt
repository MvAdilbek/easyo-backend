package kz.easy_o.usecases.module.user

import kz.easy_o.core.Page
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.core.parameter.UserParam
import kz.easy_o.usecases.common.UseCase

class UserPageUseCase(
        private val userRepository: UserRepository
) : UseCase<UserParam, Page<UserEntity>> {
    override fun execute(request: UserParam): Page<UserEntity> {
        return userRepository.findPageByParam(request)
    }

    interface UserRepository {
        fun findPageByParam(userParam: UserParam): Page<UserEntity>
    }
}