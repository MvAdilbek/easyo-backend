package kz.easy_o.usecases.module.menu

import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class MenuByIdUseCase(
        private val menuRepository: MenuRepository
) : UseCase<Long, MenuEntity> {
    override fun execute(request: Long): MenuEntity {
        return menuRepository.findById(request)
                ?: throw NotFoundException("No Menu for id: $request")
    }

    interface MenuRepository {
        fun findById(id: Long): MenuEntity?
    }
}