package kz.easy_o.usecases.module.menu

import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.service.MailSenderService
import kz.easy_o.usecases.validator.`object`.MenuEntityValidator

class MenuSaveUseCase(
        private val menuEntityValidator: MenuEntityValidator,
        private val menuRepository: MenuRepository
) : UseCase<MenuEntity, MenuEntity> {
    override fun execute(request: MenuEntity): MenuEntity {
        menuEntityValidator.checkEntity(request)
        return menuRepository.save(request)
    }

    interface MenuRepository {
        fun save(menuEntity: MenuEntity): MenuEntity
    }
}