package kz.easy_o.usecases.module.order_line

import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.validator.`object`.OrderLineEntityValidator

class OrderLineSaveUseCase(
        private val orderLineEntityValidator: OrderLineEntityValidator,
        private val orderLineRepository: OrderLineRepository
) : UseCase<OrderLineEntity, OrderLineEntity> {
    override fun execute(request: OrderLineEntity): OrderLineEntity {
        orderLineEntityValidator.checkEntity(request)

        request.id = orderLineRepository.save(request)

        return request
    }

    interface OrderLineRepository {
        fun save(orderLineEntity: OrderLineEntity): Long
    }
}