package kz.easy_o.usecases.module.order_line

import kz.easy_o.usecases.common.UseCase

class OrderLineDeleteUseCase(
        private val orderLineRepository: OrderLineRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        orderLineRepository.deleteById(request)
    }

    interface OrderLineRepository {
        fun deleteById(id: Long)
    }
}