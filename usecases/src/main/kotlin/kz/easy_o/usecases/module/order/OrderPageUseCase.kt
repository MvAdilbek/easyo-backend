package kz.easy_o.usecases.module.order

import kz.easy_o.core.Page
import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.parameter.OrderParam
import kz.easy_o.usecases.common.UseCase

class OrderPageUseCase(
        private val orderRepository: OrderRepository
) : UseCase<OrderParam, Page<OrderEntity>> {
    override fun execute(request: OrderParam): Page<OrderEntity> {
        return orderRepository.findPageByParam(request)
    }

    interface OrderRepository {
        fun findPageByParam(orderParam: OrderParam): Page<OrderEntity>
    }
}