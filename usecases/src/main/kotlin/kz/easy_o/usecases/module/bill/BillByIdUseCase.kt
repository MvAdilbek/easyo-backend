package kz.easy_o.usecases.module.bill

import kz.easy_o.core.entity.BillEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class BillByIdUseCase(
        private val billRepository: BillRepository
) : UseCase<Long, BillEntity> {
    override fun execute(request: Long): BillEntity {
        return billRepository.findById(request)
                ?: throw NotFoundException("No Bill for id: $request")
    }

    interface BillRepository {
        fun findById(id: Long): BillEntity?
    }
}