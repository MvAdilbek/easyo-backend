package kz.easy_o.usecases.module.session

import kz.easy_o.core.Page
import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.parameter.SessionParam
import kz.easy_o.usecases.common.UseCase

class SessionPageUseCase(
        private val sessionRepository: SessionRepository
) : UseCase<SessionParam, Page<SessionEntity>> {
    override fun execute(request: SessionParam): Page<SessionEntity> {
        return sessionRepository.pageByParam(request)
    }

    interface SessionRepository {
        fun pageByParam(sessionParam: SessionParam): Page<SessionEntity>
    }
}