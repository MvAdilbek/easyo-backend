package kz.easy_o.usecases.module.order

import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.service.NotificationService
import kz.easy_o.usecases.validator.`object`.OrderEntityValidator

class OrderSaveUseCase(
        private val orderEntityValidator: OrderEntityValidator,
        private val orderRepository: OrderRepository
) : UseCase<OrderEntity, OrderEntity> {
    override fun execute(request: OrderEntity): OrderEntity {
        orderEntityValidator.checkEntity(request)

        return orderRepository.save(request)
    }

    interface OrderRepository {
        fun save(orderEntity: OrderEntity): OrderEntity
    }
}