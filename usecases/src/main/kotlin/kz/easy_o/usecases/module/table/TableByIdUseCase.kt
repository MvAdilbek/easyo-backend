package kz.easy_o.usecases.module.table

import kz.easy_o.core.entity.TableEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class TableByIdUseCase(
        private val tableRepository: TableRepository
) : UseCase<Long, TableEntity> {
    override fun execute(request: Long): TableEntity {
        return tableRepository.findById(request)
                ?: throw  NotFoundException("No Table for id: $request")
    }

    interface TableRepository {
        fun findById(id: Long): TableEntity?
    }
}