package kz.easy_o.usecases.service

interface NotificationService {
    fun notify(destination: String, payload: Any)
    fun notify(destination: String, payload: String, sessionId: String)
}
