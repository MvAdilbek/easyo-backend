package kz.easy_o.usecases.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.service.NotificationService
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessageType
import org.springframework.messaging.simp.SimpMessagingTemplate

class NotificationServiceImpl(
        private val simpMessagingTemplate: SimpMessagingTemplate,
        private val objectMapper: ObjectMapper
) : NotificationService {

    private val logger = logger<NotificationServiceImpl>()

    override fun notify(destination: String, payload: String, sessionId: String) {
        logger.info("Notify to destination: $destination")
        simpMessagingTemplate.convertAndSendToUser(sessionId, destination, payload, createMessageHeaders(sessionId))
    }

    override fun notify(destination: String, payload: Any) {
        logger.info("Notify to destination: $destination")
        val json = objectMapper.writeValueAsString(payload)
        simpMessagingTemplate.convertAndSend(destination, json)
    }

    private fun createMessageHeaders(sessionId: String): MessageHeaders {
        val messageHeaderAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE)
        messageHeaderAccessor.sessionId = sessionId
        messageHeaderAccessor.setLeaveMutable(true)
        return messageHeaderAccessor.messageHeaders
    }
}