package kz.easy_o.usecases.service

import kz.easy_o.core.GoogleStorageFileConfig

interface GoogleStorageService {
    fun upload(filename: String, byteArray: ByteArray): GoogleStorageFileConfig
}