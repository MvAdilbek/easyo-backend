package kz.easy_o.usecases.service

import kz.easy_o.usecases.service.entity.NetworkResponse
import org.springframework.http.HttpMethod

interface NetworkService {
    fun request(url: String,
                method: HttpMethod,
                header: Map<String, Any>,
                body: Map<String, Any>): NetworkResponse

    fun request(url: String,
                method: HttpMethod,
                body: Map<String, Any>): NetworkResponse {
        return this.request(url, method, header = mapOf(), body = body)
    }
}