package kz.easy_o.usecases.service.impl

import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import kz.easy_o.core.GoogleStorageFileConfig
import kz.easy_o.usecases.service.GoogleStorageService

class GoogleStorageServiceImpl(
        private val storage: Storage
) : GoogleStorageService {

    override fun upload(filename: String, byteArray: ByteArray): GoogleStorageFileConfig {
        val blobId = BlobId.of("easyo", filename)
        val blobInfo = BlobInfo.newBuilder(blobId).build()
        val blob = storage.create(blobInfo, byteArray)
        return GoogleStorageFileConfig(mediaLink = blob.mediaLink)
    }
}