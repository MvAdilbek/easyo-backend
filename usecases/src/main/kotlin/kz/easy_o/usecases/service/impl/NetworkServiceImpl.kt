package kz.easy_o.usecases.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.service.NetworkService
import kz.easy_o.usecases.service.entity.NetworkResponse
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import org.springframework.web.client.HttpClientErrorException as HttpClientErrorException1

class NetworkServiceImpl(
        private val restTemplate: RestTemplate
) : NetworkService {

    private val logger = logger<NetworkServiceImpl>()

    override fun request(url: String,
                         method: HttpMethod,
                         header: Map<String, Any>,
                         body: Map<String, Any>): NetworkResponse {
        try {
            val httpHeaders = HttpHeaders()

            header.forEach { (key, value) ->
                httpHeaders.addAll(key, listOf(value.toString()))
            }

            val httpEntity: HttpEntity<String>
            var uri = url

            if (method != HttpMethod.GET) {
                httpHeaders.contentType = MediaType.APPLICATION_JSON
                httpHeaders.accept = listOf(MediaType.APPLICATION_JSON)

                val stringBody = ObjectMapper().writeValueAsString(body)
                httpEntity = HttpEntity(stringBody, httpHeaders)

                logger.info("Request body: $stringBody")
            }
            else {
                httpEntity = HttpEntity(httpHeaders)

                var fromHttpUrl = UriComponentsBuilder.fromHttpUrl(url)

                for (p in body) {
                    val value = p.value.toString()
                    fromHttpUrl = fromHttpUrl.queryParam(p.key, value)
                }
                uri = fromHttpUrl.toUriString()
            }

            logger.info("Exchange request to uri: $uri")

            val exchange = restTemplate.exchange(uri, method, httpEntity, String::class.java)

            val response = NetworkResponse(exchange.body,
                                           null,
                                           exchange.statusCode)

            logger.info("Response: $response")

            return response
        } catch (ex: HttpClientErrorException1) {
            val response = NetworkResponse(null, ex, ex.statusCode)
            logger.warn("Response: $response")
            return response
        }
    }
}