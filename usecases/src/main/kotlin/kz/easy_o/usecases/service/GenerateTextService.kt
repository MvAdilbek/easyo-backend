package kz.easy_o.usecases.service

interface GenerateTextService {
    fun getRandomToken(size: Int): String
    fun getRandomCode(size: Int): String
}