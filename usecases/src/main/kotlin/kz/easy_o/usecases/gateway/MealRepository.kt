package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.meal.MealByIdUseCase
import kz.easy_o.usecases.module.meal.MealDeleteUseCase
import kz.easy_o.usecases.module.meal.MealPageUseCase
import kz.easy_o.usecases.module.meal.MealSaveUseCase

interface MealRepository :
        MealByIdUseCase.MealRepository,
        MealDeleteUseCase.MealRepository,
        MealPageUseCase.MealRepository,
        MealSaveUseCase.MealRepository