package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.session.*

interface SessionRepository :
        SessionSaveUseCase.SessionRepository,
        SessionAuthUseCase.SessionRepository,
        SessionPageUseCase.SessionRepository,
        SessionByIdUseCase.SessionRepository,
        SessionDeleteUseCase.SessionRepository,
        SessionSocketRegisterUseCase.SessionRepository