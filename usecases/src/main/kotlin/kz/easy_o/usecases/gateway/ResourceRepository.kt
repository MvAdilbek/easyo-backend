package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.resource.ResourceByIdUseCase
import kz.easy_o.usecases.module.resource.ResourceDeleteUseCase
import kz.easy_o.usecases.module.resource.ResourcePageUseCase
import kz.easy_o.usecases.module.resource.ResourceSaveUseCase

interface ResourceRepository :
        ResourceByIdUseCase.ResourceRepository,
        ResourcePageUseCase.ResourceRepository,
        ResourceSaveUseCase.ResourceRepository,
        ResourceDeleteUseCase.ResourceRepository