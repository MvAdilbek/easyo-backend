package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.menu.MenuByIdUseCase
import kz.easy_o.usecases.module.menu.MenuDeleteUseCase
import kz.easy_o.usecases.module.menu.MenuPageUseCase
import kz.easy_o.usecases.module.menu.MenuSaveUseCase

interface MenuRepository :
        MenuByIdUseCase.MenuRepository,
        MenuDeleteUseCase.MenuRepository,
        MenuPageUseCase.MenuRepository,
        MenuSaveUseCase.MenuRepository