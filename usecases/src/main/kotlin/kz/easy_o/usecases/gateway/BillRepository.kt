package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.bill.BillByIdUseCase
import kz.easy_o.usecases.module.bill.BillDeleteUseCase
import kz.easy_o.usecases.module.bill.BillPageUseCase
import kz.easy_o.usecases.module.bill.BillSaveUseCase

interface BillRepository :
        BillByIdUseCase.BillRepository,
        BillDeleteUseCase.BillRepository,
        BillPageUseCase.BillRepository,
        BillSaveUseCase.BillRepository