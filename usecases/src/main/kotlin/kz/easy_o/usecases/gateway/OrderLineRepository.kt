package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.order_line.OrderLineByIdUseCase
import kz.easy_o.usecases.module.order_line.OrderLineDeleteUseCase
import kz.easy_o.usecases.module.order_line.OrderLinePageUseCase
import kz.easy_o.usecases.module.order_line.OrderLineSaveUseCase

interface OrderLineRepository :
        OrderLineByIdUseCase.OrderLineRepository,
        OrderLineDeleteUseCase.OrderLineRepository,
        OrderLinePageUseCase.OrderLineRepository,
        OrderLineSaveUseCase.OrderLineRepository