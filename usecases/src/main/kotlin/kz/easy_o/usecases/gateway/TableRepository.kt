package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.table.TableByIdUseCase
import kz.easy_o.usecases.module.table.TableDeleteUseCase
import kz.easy_o.usecases.module.table.TablePageUseCase
import kz.easy_o.usecases.module.table.TableSaveUseCase

interface TableRepository :
        TableByIdUseCase.TableRepository,
        TableDeleteUseCase.TableRepository,
        TablePageUseCase.TableRepository,
        TableSaveUseCase.TableRepository