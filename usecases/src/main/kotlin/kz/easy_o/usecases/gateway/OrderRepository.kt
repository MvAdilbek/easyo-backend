package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.order.OrderByIdUseCase
import kz.easy_o.usecases.module.order.OrderDeleteUseCase
import kz.easy_o.usecases.module.order.OrderPageUseCase
import kz.easy_o.usecases.module.order.OrderSaveUseCase

interface OrderRepository :
        OrderByIdUseCase.OrderRepository,
        OrderDeleteUseCase.OrderRepository,
        OrderPageUseCase.OrderRepository,
        OrderSaveUseCase.OrderRepository