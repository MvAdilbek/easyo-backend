package kz.easy_o.usecases.exception

import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError

class FieldsException(
        val fieldErrorList: List<FieldError>
) : RuntimeException() {

    companion object {
        fun processValidationError(result: BindingResult): FieldsException {
            val fieldErrors = result.fieldErrors
            return FieldsException(fieldErrors)
        }
    }
}