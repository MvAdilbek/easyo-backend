package kz.easy_o.usecases.exception

class DataIntegrityException(message: String) : RuntimeException(message)