package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.UserEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import kz.easy_o.usecases.validator.unit.EmailAddressValidator
import org.springframework.validation.Errors

class UserEntityValidator : ObjectValidator<UserEntity> {

    override val cls: Class<UserEntity>
        get() = UserEntity::class.java

    private val logger = logger<UserEntityValidator>()

    companion object {
        const val FIRST_NAME = "firstName"
        const val LAST_NAME = "lastName"
        const val EMAIL = "email"
        const val USER_POSITION_ENUM = "userPositionEnum"
    }

    override fun validateEntity(entity: UserEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(FIRST_NAME, ErrorCode.FIRST_NAME_IS_NULL)
        errors.rejectIfNull(LAST_NAME, ErrorCode.LAST_NAME_IS_NULL)
        errors.rejectIfNull(EMAIL, ErrorCode.EMAIL_IS_NULL)
        errors.rejectIfNull(USER_POSITION_ENUM, ErrorCode.USER_POSITION_ENUM_IS_NULL)

        if (entity.email != null
                && EmailAddressValidator.nonValid(entity.email!!)) {
            errors.reject(EMAIL, ErrorCode.INVALID_EMAIL)
        }

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val FIRST_NAME_IS_NULL = "userEntity.firstNameIsNull"
        const val LAST_NAME_IS_NULL = "userEntity.lastNameIsNull"
        const val EMAIL_IS_NULL = "userEntity.emailIsNull"
        const val INVALID_EMAIL = "userEntity.invalidEmail"
        const val USER_POSITION_ENUM_IS_NULL = "userEntity.userPositionEnumIsNull"
    }
}