package kz.easy_o.usecases.validator.unit

import kz.easy_o.usecases.validator.unit.intf.TextValidator

object IPAddressValidator : TextValidator {

    override fun valid(obj: String): Boolean {
        return isRegex(obj, "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\$".toRegex())
    }
}