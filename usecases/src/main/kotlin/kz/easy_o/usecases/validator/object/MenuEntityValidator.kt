package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class MenuEntityValidator : ObjectValidator<MenuEntity> {

    override val cls: Class<MenuEntity>
        get() = MenuEntity::class.java

    private val logger = logger<MenuEntityValidator>()

    companion object {
        const val MENU_TYPE = "menuType"
        const val FROM_DATE = "fromDate"
    }

    override fun validateEntity(entity: MenuEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(MENU_TYPE, ErrorCode.MENU_TYPE_IS_NULL)
        errors.rejectIfNull(FROM_DATE, ErrorCode.FROM_DATE_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val MENU_TYPE_IS_NULL = "menuEntity.menuTypeIsNull"
        const val FROM_DATE_IS_NULL = "menuEntity.fromDateIsNull"
    }
}