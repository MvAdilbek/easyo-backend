package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.TableEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class TableEntityValidator : ObjectValidator<TableEntity> {
    override val cls: Class<TableEntity>
        get() = TableEntity::class.java

    private val logger = logger<TableEntityValidator>()

    companion object {
        const val SIZE = "size"
        const val RESERVED = "reserved"
        const val NUMBER = "number"
    }

    override fun validateEntity(entity: TableEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(SIZE, ErrorCode.SIZE_IS_NULL)
        errors.rejectIfNull(RESERVED, ErrorCode.RESERVED_IS_NULL)
        errors.rejectIfNull(NUMBER, ErrorCode.NUMBER_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val SIZE_IS_NULL = "size"
        const val RESERVED_IS_NULL = "reserved"
        const val NUMBER_IS_NULL = "number"
    }
}