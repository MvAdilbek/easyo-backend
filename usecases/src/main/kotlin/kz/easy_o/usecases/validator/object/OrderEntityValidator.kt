package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class OrderEntityValidator : ObjectValidator<OrderEntity> {

    override val cls: Class<OrderEntity>
        get() = OrderEntity::class.java

    private val logger = logger<OrderEntityValidator>()

    companion object {
        const val ORDERED_DATE = "orderedDate"
        const val TABLE_ENTITY_ID = "tableEntity.id"
        const val ACCEPTED_USER_ENTITY_ID = "acceptedUserEntity.id"
    }

    override fun validateEntity(entity: OrderEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(ORDERED_DATE, ErrorCode.ORDERED_DATE_IS_NULL)
        errors.rejectIfNull(TABLE_ENTITY_ID, ErrorCode.TABLE_ENTITY_ID_IS_NULL)
        errors.rejectIfNull(ACCEPTED_USER_ENTITY_ID, ErrorCode.ACCEPTED_USER_ENTITY_ID_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val ORDERED_DATE_IS_NULL = "orderEntity.orderedDateIsNull"
        const val TABLE_ENTITY_ID_IS_NULL = "orderEntity.tableEntityIdIsNull"
        const val ACCEPTED_USER_ENTITY_ID_IS_NULL = "orderEntity.acceptedUserEntityIdIsNull"
    }
}