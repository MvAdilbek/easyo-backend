package kz.easy_o.usecases.validator.unit.intf

interface UnitValidator<T> {
    fun valid(obj: T): Boolean

    fun nonValid(obj: T): Boolean {
        return valid(obj).not()
    }
}