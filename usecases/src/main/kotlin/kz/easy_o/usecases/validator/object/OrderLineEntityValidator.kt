package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class OrderLineEntityValidator : ObjectValidator<OrderLineEntity> {

    override val cls: Class<OrderLineEntity>
        get() = OrderLineEntity::class.java

    private val logger = logger<OrderLineEntityValidator>()

    companion object {
        const val QUANTITY = "quantity"
        const val UNIT_PRICE = "unitPrice"
        const val MEAL_ENTITY_ID = "mealEntity.id"
        const val ORDER_ENTITY_ID = "orderEntity.id"
        const val STATUS = "status"
        const val ACCEPTED_USER_ENTITY_ID = "acceptedUserEntity.id"
    }

    override fun validateEntity(entity: OrderLineEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(QUANTITY, ErrorCode.QUANTITY_IS_NULL)
        errors.rejectIfNull(UNIT_PRICE, ErrorCode.UNIT_PRICE_IS_NULL)
        errors.rejectIfNull(MEAL_ENTITY_ID, ErrorCode.MEAL_ENTITY_ID_IS_NULL)
        errors.rejectIfNull(ORDER_ENTITY_ID, ErrorCode.ORDER_ENTITY_ID_IS_NULL)
        errors.rejectIfNull(STATUS, ErrorCode.STATUS_IS_NULL)
        errors.rejectIfNull(ACCEPTED_USER_ENTITY_ID, ErrorCode.ACCEPTED_USER_ENTITY_ID_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val QUANTITY_IS_NULL = "orderLineEntity.quantityIsNull"
        const val UNIT_PRICE_IS_NULL = "orderLineEntity.unitPriceIsNull"
        const val MEAL_ENTITY_ID_IS_NULL = "orderLineEntity.mealEntityIdIsNull"
        const val ORDER_ENTITY_ID_IS_NULL = "orderLineEntity.orderEntityIdIsNull"
        const val STATUS_IS_NULL = "orderLineEntity.statusIsNull"
        const val ACCEPTED_USER_ENTITY_ID_IS_NULL = "orderLineEntity.acceptedUserEntityIdIsNull"
    }
}