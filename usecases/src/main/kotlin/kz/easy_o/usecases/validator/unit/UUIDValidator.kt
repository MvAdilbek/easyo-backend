package kz.easy_o.usecases.validator.unit

import kz.easy_o.usecases.validator.unit.intf.TextValidator

object UUIDValidator : TextValidator {

    override fun valid(obj: String): Boolean {
        return isRegex(obj, "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$".toRegex())
    }
}