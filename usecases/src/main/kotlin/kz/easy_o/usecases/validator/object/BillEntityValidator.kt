package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.BillEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class BillEntityValidator : ObjectValidator<BillEntity> {

    override val cls: Class<BillEntity>
        get() = BillEntity::class.java

    private val logger = logger<BillEntityValidator>()

    companion object {
        const val BILL_TIME = "billTime"
        const val AMOUNT = "amount"
        const val ORDER_ID = "orderEntity.id"
        const val ACCEPTED_USER_ID = "acceptedUserEntity.id"
    }

    override fun validateEntity(entity: BillEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(BILL_TIME, ErrorCode.BILL_TIME_IS_NULL)
        errors.rejectIfNull(AMOUNT, ErrorCode.AMOUNT_IS_NULL)
        errors.rejectIfNull(ORDER_ID, ErrorCode.ORDER_ID_IS_NULL)
        errors.rejectIfNull(ACCEPTED_USER_ID, ErrorCode.ACCEPTED_USER_ID_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val BILL_TIME_IS_NULL = "mealEntity.billTimeIsNull"
        const val AMOUNT_IS_NULL = "mealEntity.amountIsNull"
        const val ORDER_ID_IS_NULL = "mealEntity.orderEntityIdIsNull"
        const val ACCEPTED_USER_ID_IS_NULL = "mealEntity.acceptedUserEntityIdIsNull"
    }
}