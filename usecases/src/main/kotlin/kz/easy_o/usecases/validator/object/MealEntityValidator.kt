package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class MealEntityValidator : ObjectValidator<MealEntity> {

    override val cls: Class<MealEntity>
        get() = MealEntity::class.java

    private val logger = logger<MealEntityValidator>()

    companion object {
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val UNIT_PRICE = "unitPrice"
        const val SIZE = "size"
        const val MENU_ENTITY_ID = "menuEntity.id"
    }

    override fun validateEntity(entity: MealEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(NAME, ErrorCode.NAME_IS_NULL)
        errors.rejectIfNull(DESCRIPTION, ErrorCode.DESCRIPTION_IS_NULL)
        errors.rejectIfNull(UNIT_PRICE, ErrorCode.UNIT_PRICE_IS_NULL)
        errors.rejectIfNull(SIZE, ErrorCode.SIZE_IS_NULL)
        errors.rejectIfNull(MENU_ENTITY_ID, ErrorCode.MENU_ENTITY_ID_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val NAME_IS_NULL = "mealEntity.nameIsNull"
        const val DESCRIPTION_IS_NULL = "mealEntity.descriptionIsNull"
        const val UNIT_PRICE_IS_NULL = "mealEntity.unitPriceIsNull"
        const val SIZE_IS_NULL = "mealEntity.sizeIsNull"
        const val MENU_ENTITY_ID_IS_NULL = "mealEntity.menuEntityIdIsNull"
    }
}