package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class ResourceEntityValidator : ObjectValidator<ResourceEntity> {
    override val cls: Class<ResourceEntity>
        get() = ResourceEntity::class.java

    private val logger = logger<ResourceEntityValidator>()

    companion object {
        const val RESOURCE_URL = "resourceUrl"
        const val RESOURCE_TYPE_ENUM = "resourceTypeEnum"
    }

    override fun validateEntity(entity: ResourceEntity, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(RESOURCE_URL, ErrorCode.RESOURCE_URL_IS_NULL)
        errors.rejectIfNull(RESOURCE_TYPE_ENUM, ErrorCode.RESOURCE_TYPE_ENUM_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val RESOURCE_URL_IS_NULL = "request.resourceUrlIsNull"
        const val RESOURCE_TYPE_ENUM_IS_NULL = "request.resourceTypeEnumIsNull"
    }
}