package kz.easy_o.usecases.validator.`object`

import kz.easy_o.core.parameter.session.SessionSaveParam
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.extension.rejectIfNull
import kz.easy_o.usecases.validator.`object`.intf.ObjectValidator
import org.springframework.validation.Errors

class SessionSaveParamValidator :
        ObjectValidator<SessionSaveParam> {

    override val cls: Class<SessionSaveParam>
        get() = SessionSaveParam::class.java

    private val logger = logger<SessionSaveParamValidator>()

    companion object {
        const val LOGIN = "login"
        const val PASSWORD = "password"
    }

    override fun validateEntity(entity: SessionSaveParam, errors: Errors) {
        logger.info("Validate entity: $entity")

        errors.rejectIfNull(LOGIN,
                            ErrorCode.LOGIN_IS_NULL)
        errors.rejectIfNull(PASSWORD,
                            ErrorCode.PASSWORD_IS_NULL)

        if (errors.hasErrors()) {
            logger.warn("Find error with count: ${errors.errorCount}")
        }
    }

    object ErrorCode {
        const val LOGIN_IS_NULL = "sessionSaveParam.loginIsNull"
        const val PASSWORD_IS_NULL = "sessionSaveParam.passwordIsNull"
    }
}