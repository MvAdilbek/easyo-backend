package kz.easy_o.usecases.validator.unit

import kz.easy_o.usecases.validator.unit.intf.TextValidator

object LinkValidator : TextValidator {

    override fun valid(obj: String): Boolean {
        return isRegex(obj,
                       ("(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}" +
                               "|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www" +
                               "\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})").toRegex())
    }
}