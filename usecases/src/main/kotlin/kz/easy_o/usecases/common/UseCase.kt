package kz.easy_o.usecases.common

import org.springframework.security.concurrent.DelegatingSecurityContextExecutor
import org.springframework.security.core.context.SecurityContextHolder
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.concurrent.Executors

interface UseCase<in Request, out Response> {
    fun execute(request: Request): Response
}

interface UseCaseExecutor {
    operator fun <RequestDto, ResponseDto, Request, Response> invoke(
            useCase: UseCase<Request, Response>,
            requestDto: RequestDto,
            requestConverter: (RequestDto) -> Request,
            responseConverter: (Response) -> ResponseDto
    ): CompletionStage<ResponseDto>

    operator fun <RequestDto, Request> invoke(
            useCase: UseCase<Request, Unit>,
            requestDto: RequestDto,
            requestConverter: (RequestDto) -> Request
    ) = invoke(useCase, requestDto, requestConverter, {})

    operator fun <ResponseDto, Response> invoke(
            useCase: UseCase<Unit, Response>,
            responseConverter: (Response) -> ResponseDto
    ) = invoke(useCase, Unit, { }, responseConverter)

    operator fun invoke(useCase: UseCase<Unit, Unit>) = invoke(useCase, Unit) { }
}

class UseCaseExecutorImpl : UseCaseExecutor {
    override fun <RequestDto, ResponseDto, Request, Response> invoke(
            useCase: UseCase<Request, Response>,
            requestDto: RequestDto,
            requestConverter: (RequestDto) -> Request,
            responseConverter: (Response) -> ResponseDto
    ): CompletionStage<ResponseDto> {
        val securityContext = SecurityContextHolder.getContext()
        val delegatedExecutor = Executors.newCachedThreadPool()
        val delegatingExecutor = DelegatingSecurityContextExecutor(delegatedExecutor, securityContext)

        return CompletableFuture
                .supplyAsync({ requestConverter(requestDto) }, delegatingExecutor::execute)
                .thenApplyAsync({ useCase.execute(it) }, delegatingExecutor::execute)
                .thenApplyAsync({ responseConverter(it) }, delegatingExecutor::execute)
    }
}
