package kz.easy_o.delivery.dto

import kz.easy_o.core.enums.OrderLineStatusEnum
import kz.easy_o.delivery.dto.base.BaseDto
import java.math.BigDecimal

data class OrderLineDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var quantity: Int? = null,
        var unitPrice: BigDecimal? = null,
        var meal: MealDto? = null,
        var order: OrderDto? = null,
        var status: OrderLineStatusEnum? = null,
        var acceptedUser: UserDto? = null
) : BaseDto()