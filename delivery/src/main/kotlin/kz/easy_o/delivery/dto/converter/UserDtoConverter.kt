package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.UserEntity
import kz.easy_o.delivery.dto.UserDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object UserDtoConverter : DtoConverter<UserEntity, UserDto> {

    override fun toDto(entity: UserEntity): UserDto {
        val userDto = UserDto()

        userDto.id = entity.id
        userDto.deleted = entity.deleted
        userDto.imageUrl = entity.imageUrl
        userDto.firstName = entity.firstName
        userDto.lastName = entity.lastName
        userDto.email = entity.email
        userDto.phoneNumber = entity.phoneNumber
        userDto.userPositionEnum = entity.userPositionEnum

        return userDto
    }

    override fun toEntity(dto: UserDto): UserEntity {
        val userEntity = UserEntity()

        userEntity.id = dto.id
        userEntity.deleted = dto.deleted
        userEntity.imageUrl = dto.imageUrl
        userEntity.firstName = dto.firstName
        userEntity.lastName = dto.lastName
        userEntity.email = dto.email
        userEntity.password = dto.password
        userEntity.phoneNumber = dto.phoneNumber
        userEntity.userPositionEnum = dto.userPositionEnum

        return userEntity
    }
}