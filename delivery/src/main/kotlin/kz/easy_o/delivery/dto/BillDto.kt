package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto
import java.math.BigDecimal
import java.time.LocalDateTime

data class BillDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var billTime: LocalDateTime? = null,
        var amount: BigDecimal? = null,
        var order: OrderDto? = null,
        var acceptedUser: UserDto? = null
) : BaseDto()