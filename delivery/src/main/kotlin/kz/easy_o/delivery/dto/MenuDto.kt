package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto
import java.time.LocalDate

data class MenuDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var menuType: String? = null,
        var fromDate: LocalDate? = null,
        var toDate: LocalDate? = null,
        var mealList: List<MealDto>? = null
) : BaseDto()