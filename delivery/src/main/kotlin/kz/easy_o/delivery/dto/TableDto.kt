package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto

data class TableDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var size: Int? = null,
        var reserved: Boolean? = null,
        var number: String? = null,

        var order: OrderDto? = null
) : BaseDto()