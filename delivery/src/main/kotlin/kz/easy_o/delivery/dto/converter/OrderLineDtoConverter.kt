package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.delivery.dto.OrderLineDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object OrderLineDtoConverter : DtoConverter<OrderLineEntity, OrderLineDto> {

    override fun toDto(entity: OrderLineEntity): OrderLineDto {
        val orderDto = OrderLineDto()

        orderDto.id = entity.id
        orderDto.deleted = entity.deleted
        orderDto.quantity = entity.quantity
        orderDto.unitPrice = entity.unitPrice
        orderDto.meal = entity.mealEntity?.let(MealDtoConverter::toDto)
        orderDto.order = entity.orderEntity?.let(OrderDtoConverter::toDto)
        orderDto.status = entity.status
        orderDto.acceptedUser = entity.acceptedUserEntity?.let(UserDtoConverter::toDto)

        return orderDto
    }

    override fun toEntity(dto: OrderLineDto): OrderLineEntity {
        val orderEntity = OrderLineEntity()

        orderEntity.id = dto.id
        orderEntity.deleted = dto.deleted
        orderEntity.quantity = dto.quantity
        orderEntity.unitPrice = dto.unitPrice
        orderEntity.mealEntity = dto.meal?.let(MealDtoConverter::toEntity)
        orderEntity.orderEntity = dto.order?.let(OrderDtoConverter::toEntity)
        orderEntity.status = dto.status
        orderEntity.acceptedUserEntity = dto.acceptedUser?.let(UserDtoConverter::toEntity)

        return orderEntity
    }
}