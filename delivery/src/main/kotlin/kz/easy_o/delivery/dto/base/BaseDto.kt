package kz.easy_o.delivery.dto.base

open class BaseDto {
    open var id: Long? = null
    open var deleted: Long? = null
}