package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.delivery.dto.OrderDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object OrderDtoConverter : DtoConverter<OrderEntity, OrderDto> {

    override fun toDto(entity: OrderEntity): OrderDto {
        val orderDto = OrderDto()

        orderDto.id = entity.id
        orderDto.deleted = entity.deleted
        orderDto.orderedDate = entity.orderedDate
        orderDto.table = entity.tableEntity?.let(TableDtoConverter::toDto)
        orderDto.acceptedUser = entity.acceptedUserEntity?.let(UserDtoConverter::toDto)
        orderDto.orderLineList = entity.orderLineEntityList?.map(OrderLineDtoConverter::toDto)
        orderDto.comment = entity.comment

        return orderDto
    }

    override fun toEntity(dto: OrderDto): OrderEntity {
        val orderEntity = OrderEntity()

        orderEntity.id = dto.id
        orderEntity.deleted = dto.deleted
        orderEntity.orderedDate = dto.orderedDate
        orderEntity.tableEntity = dto.table?.let(TableDtoConverter::toEntity)
        orderEntity.acceptedUserEntity = dto.acceptedUser?.let(UserDtoConverter::toEntity)
        orderEntity.orderLineEntityList = dto.orderLineList?.map(OrderLineDtoConverter::toEntity)
        orderEntity.comment = dto.comment

        return orderEntity
    }
}