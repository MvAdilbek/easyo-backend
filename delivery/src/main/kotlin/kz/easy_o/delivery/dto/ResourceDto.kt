package kz.easy_o.delivery.dto

import kz.easy_o.core.enums.ResourceTypeEnum
import kz.easy_o.delivery.dto.base.BaseDto

data class ResourceDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var resourceUrl: String? = null,
        var resourceTypeEnum: ResourceTypeEnum? = null
) : BaseDto()