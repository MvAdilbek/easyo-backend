package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto
import java.math.BigDecimal
import java.time.LocalDate

data class MealDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var name: String? = null,
        var description: String? = null,
        var unitPrice: BigDecimal? = null,
        var size: Int? = null,
        var imageUrl: String? = null,
        var menu: MenuDto? = null
) : BaseDto()