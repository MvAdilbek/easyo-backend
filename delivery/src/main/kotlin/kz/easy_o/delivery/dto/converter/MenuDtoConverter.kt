package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.delivery.dto.MenuDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object MenuDtoConverter : DtoConverter<MenuEntity, MenuDto> {

    override fun toDto(entity: MenuEntity): MenuDto {
        val menuDto = MenuDto()

        menuDto.id = entity.id
        menuDto.deleted = entity.deleted
        menuDto.menuType = entity.menuType
        menuDto.fromDate = entity.fromDate
        menuDto.toDate = entity.toDate
        menuDto.mealList = entity.mealEntityList?.map(MealDtoConverter::toDto)

        return menuDto
    }

    override fun toEntity(dto: MenuDto): MenuEntity {
        val menuEntity = MenuEntity()

        menuEntity.id = dto.id
        menuEntity.menuType = dto.menuType
        menuEntity.fromDate = dto.fromDate
        menuEntity.toDate = dto.toDate

        return menuEntity
    }
}