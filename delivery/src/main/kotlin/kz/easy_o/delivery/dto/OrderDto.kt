package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto
import java.time.LocalDateTime

data class OrderDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var orderedDate: LocalDateTime? = null,
        var table: TableDto? = null,
        var acceptedUser: UserDto? = null,
        var comment: String? = null,
        var orderLineList: List<OrderLineDto>? = null
) : BaseDto()