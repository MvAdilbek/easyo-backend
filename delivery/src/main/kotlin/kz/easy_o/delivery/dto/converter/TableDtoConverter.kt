package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.TableEntity
import kz.easy_o.delivery.dto.TableDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object TableDtoConverter : DtoConverter<TableEntity, TableDto> {

    override fun toDto(entity: TableEntity): TableDto {
        val tableDto = TableDto()

        tableDto.id = entity.id
        tableDto.deleted = entity.deleted
        tableDto.size = entity.size
        tableDto.reserved = entity.reserved
        tableDto.number = entity.number
        tableDto.order = entity.activeOrderEntity?.let(OrderDtoConverter::toDto)

        return tableDto
    }

    override fun toEntity(dto: TableDto): TableEntity {
        val tableEntity = TableEntity()

        tableEntity.id = dto.id
        tableEntity.deleted = dto.deleted
        tableEntity.size = dto.size
        tableEntity.reserved = dto.reserved
        tableEntity.number = dto.number

        return tableEntity
    }
}