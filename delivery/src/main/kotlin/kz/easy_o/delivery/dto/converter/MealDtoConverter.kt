package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.delivery.dto.MealDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object MealDtoConverter : DtoConverter<MealEntity, MealDto> {

    override fun toDto(entity: MealEntity): MealDto {
        val menuDto = MealDto()

        menuDto.id = entity.id
        menuDto.deleted = entity.deleted
        menuDto.name = entity.name
        menuDto.description = entity.description
        menuDto.unitPrice = entity.unitPrice
        menuDto.size = entity.size
        menuDto.imageUrl = entity.imageUrl
        menuDto.menu = entity.menuEntity?.let(MenuDtoConverter::toDto)

        return menuDto
    }

    override fun toEntity(dto: MealDto): MealEntity {
        val menuEntity = MealEntity()

        menuEntity.id = dto.id
        menuEntity.name = dto.name
        menuEntity.description = dto.description
        menuEntity.unitPrice = dto.unitPrice
        menuEntity.size = dto.size
        menuEntity.imageUrl = dto.imageUrl
        menuEntity.menuEntity = dto.menu?.let(MenuDtoConverter::toEntity)

        return menuEntity
    }
}