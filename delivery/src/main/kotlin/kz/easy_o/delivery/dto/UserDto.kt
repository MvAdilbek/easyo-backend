package kz.easy_o.delivery.dto

import kz.easy_o.core.enums.UserPositionEnum
import kz.easy_o.delivery.dto.base.BaseDto

data class UserDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var imageUrl: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null,
        var password: String? = null,
        var phoneNumber: String? = null,
        var userPositionEnum: UserPositionEnum? = null
) : BaseDto()