package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.BillEntity
import kz.easy_o.delivery.dto.BillDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object BillDtoConverter : DtoConverter<BillEntity, BillDto> {

    override fun toDto(entity: BillEntity): BillDto {
        val billDto = BillDto()

        billDto.id = entity.id
        billDto.deleted = entity.deleted
        billDto.billTime = entity.billTime
        billDto.amount = entity.amount
        billDto.order = entity.orderEntity?.let(OrderDtoConverter::toDto)
        billDto.acceptedUser = entity.acceptedUserEntity?.let(UserDtoConverter::toDto)

        return billDto
    }

    override fun toEntity(dto: BillDto): BillEntity {
        val billEntity = BillEntity()

        billEntity.id = dto.id
        billEntity.billTime = dto.billTime
        billEntity.amount = dto.amount
        billEntity.orderEntity = dto.order?.let(OrderDtoConverter::toEntity)
        billEntity.acceptedUserEntity = dto.acceptedUser?.let(UserDtoConverter::toEntity)

        return billEntity
    }
}