package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.TableParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.TableDto
import kz.easy_o.delivery.dto.converter.TableDtoConverter
import kz.easy_o.delivery.resource.TableResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.table.TableByIdUseCase
import kz.easy_o.usecases.module.table.TableDeleteUseCase
import kz.easy_o.usecases.module.table.TablePageUseCase
import kz.easy_o.usecases.module.table.TableSaveUseCase

class TableResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val tablePageUseCase: TablePageUseCase,
        private val tableByIdUseCase: TableByIdUseCase,
        private val tableSaveUseCase: TableSaveUseCase,
        private val tableDeleteUseCase: TableDeleteUseCase
) : TableResource {

    override fun page(tableParam: TableParam) =
            useCaseExecutor(useCase = tablePageUseCase,
                            requestDto = tableParam,
                            requestConverter = { it },
                            responseConverter = { page -> page.map(TableDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = tableByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = TableDtoConverter::toDto)

    override fun save(tableDto: TableDto) =
            useCaseExecutor(useCase = tableSaveUseCase,
                            requestDto = tableDto,
                            requestConverter = {
                                it.id = null
                                TableDtoConverter.toEntity(it)
                            },
                            responseConverter = TableDtoConverter::toDto)

    override fun update(id: Long,
                        tableDto: TableDto) =
            useCaseExecutor(useCase = tableSaveUseCase,
                            requestDto = tableDto,
                            requestConverter = {
                                it.id = id
                                TableDtoConverter.toEntity(it)
                            },
                            responseConverter = TableDtoConverter::toDto)

    override fun delete(id: Long) =
            useCaseExecutor(useCase = tableDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}