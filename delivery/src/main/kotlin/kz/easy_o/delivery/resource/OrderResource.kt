package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.OrderParam
import kz.easy_o.delivery.dto.OrderDto
import org.springframework.http.ResponseEntity
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/orders")
interface OrderResource {

    @GetMapping
    fun page(@ModelAttribute orderParam: OrderParam): CompletionStage<Page<OrderDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<OrderDto>

    @PostMapping
    fun save(@RequestBody orderDto: OrderDto): CompletionStage<OrderDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody orderDto: OrderDto): CompletionStage<OrderDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}