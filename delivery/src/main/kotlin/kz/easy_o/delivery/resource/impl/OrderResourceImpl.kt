package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.OrderParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.OrderDto
import kz.easy_o.delivery.dto.converter.OrderDtoConverter
import kz.easy_o.delivery.resource.OrderResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.order.OrderByIdUseCase
import kz.easy_o.usecases.module.order.OrderDeleteUseCase
import kz.easy_o.usecases.module.order.OrderPageUseCase
import kz.easy_o.usecases.module.order.OrderSaveUseCase
import kz.easy_o.usecases.service.NotificationService

class OrderResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val orderPageUseCase: OrderPageUseCase,
        private val orderByIdUseCase: OrderByIdUseCase,
        private val orderSaveUseCase: OrderSaveUseCase,
        private val orderDeleteUseCase: OrderDeleteUseCase,
        private val notificationService: NotificationService
) : OrderResource {

    override fun page(orderParam: OrderParam) =
            useCaseExecutor(useCase = orderPageUseCase,
                            requestDto = orderParam,
                            requestConverter = { it },
                            responseConverter = { page -> page.map(OrderDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = orderByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = OrderDtoConverter::toDto)

    override fun save(orderDto: OrderDto) =
            useCaseExecutor(useCase = orderSaveUseCase,
                            requestDto = orderDto,
                            requestConverter = {
                                it.id = null
                                OrderDtoConverter.toEntity(it)
                            },
                            responseConverter = {
                                val result = OrderDtoConverter.toDto(it)
                                notificationService.notify("/orders/update", result)
                                result
                            })

    override fun update(id: Long,
                        orderDto: OrderDto) =
            useCaseExecutor(useCase = orderSaveUseCase,
                            requestDto = orderDto,
                            requestConverter = {
                                it.id = id
                                OrderDtoConverter.toEntity(it)
                            },
                            responseConverter = {
                                val result = OrderDtoConverter.toDto(it)
                                notificationService.notify("/orders/update", result)
                                result
                            })

    override fun delete(id: Long) =
            useCaseExecutor(useCase = orderDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}