package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.TableParam
import kz.easy_o.delivery.dto.TableDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/tables")
interface TableResource {

    @GetMapping
    fun page(@ModelAttribute tableParam: TableParam): CompletionStage<Page<TableDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<TableDto>

    @PostMapping
    fun save(@RequestBody tableDto: TableDto): CompletionStage<TableDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody tableDto: TableDto): CompletionStage<TableDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}