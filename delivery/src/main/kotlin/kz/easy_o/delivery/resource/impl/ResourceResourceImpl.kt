package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.ResourceParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.ResourceDto
import kz.easy_o.delivery.dto.converter.ResourceDtoConverter
import kz.easy_o.delivery.resource.ResourceResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.resource.ResourceByIdUseCase
import kz.easy_o.usecases.module.resource.ResourceDeleteUseCase
import kz.easy_o.usecases.module.resource.ResourcePageUseCase
import kz.easy_o.usecases.module.resource.ResourceSaveUseCase

class ResourceResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val requestPageUseCase: ResourcePageUseCase,
        private val requestByIdUseCase: ResourceByIdUseCase,
        private val requestSaveUseCase: ResourceSaveUseCase,
        private val requestDeleteUseCase: ResourceDeleteUseCase
) : ResourceResource {

    override fun page(resourceParam: ResourceParam) =
            useCaseExecutor(useCase = requestPageUseCase,
                            requestDto = resourceParam,
                            requestConverter = { it },
                            responseConverter = { it.map(ResourceDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = requestByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { ResourceDtoConverter.toDto(it) })

    override fun save(resourceDto: ResourceDto) =
            useCaseExecutor(useCase = requestSaveUseCase,
                            requestDto = resourceDto,
                            requestConverter = {
                                it.id = null
                                ResourceDtoConverter.toEntity(it)
                            },
                            responseConverter = { ResourceDtoConverter.toDto(it) })

    override fun update(id: Long,
                        resourceDto: ResourceDto) =
            useCaseExecutor(useCase = requestSaveUseCase,
                            requestDto = resourceDto,
                            requestConverter = {
                                it.id = id
                                ResourceDtoConverter.toEntity(it)
                            },
                            responseConverter = { ResourceDtoConverter.toDto(it) })

    override fun delete(id: Long) =
            useCaseExecutor(useCase = requestDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}