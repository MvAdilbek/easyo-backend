package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.StorageUploadParam
import kz.easy_o.delivery.resource.StorageResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.storage.StorageUploadUseCase
import org.springframework.web.multipart.MultipartFile

class StorageResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val storageUploadUseCase: StorageUploadUseCase
) : StorageResource {

    override fun upload(multipartFile: MultipartFile) =
            useCaseExecutor(useCase = storageUploadUseCase,
                            requestDto = null,
                            requestConverter = {
                                val filename = "${System.nanoTime()}_${multipartFile.originalFilename}"
                                StorageUploadParam(filename,
                                                   multipartFile.bytes)
                            },
                            responseConverter = { it })
}