package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.OrderLineParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.OrderLineDto
import kz.easy_o.delivery.dto.converter.OrderLineDtoConverter
import kz.easy_o.delivery.resource.OrderLineResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.order_line.OrderLineByIdUseCase
import kz.easy_o.usecases.module.order_line.OrderLineDeleteUseCase
import kz.easy_o.usecases.module.order_line.OrderLinePageUseCase
import kz.easy_o.usecases.module.order_line.OrderLineSaveUseCase
import kz.easy_o.usecases.service.NotificationService

class OrderLineResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val orderLinePageUseCase: OrderLinePageUseCase,
        private val orderLineByIdUseCase: OrderLineByIdUseCase,
        private val orderLineSaveUseCase: OrderLineSaveUseCase,
        private val orderLineDeleteUseCase: OrderLineDeleteUseCase,
        private val notificationService: NotificationService
) : OrderLineResource {

    override fun page(orderLineParam: OrderLineParam) =
            useCaseExecutor(useCase = orderLinePageUseCase,
                            requestDto = orderLineParam,
                            requestConverter = { it },
                            responseConverter = { page -> page.map(OrderLineDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = orderLineByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = OrderLineDtoConverter::toDto)

    override fun save(orderLineDto: OrderLineDto) =
            useCaseExecutor(useCase = orderLineSaveUseCase,
                            requestDto = orderLineDto,
                            requestConverter = {
                                it.id = null
                                OrderLineDtoConverter.toEntity(it)
                            },
                            responseConverter = {
                                val result = OrderLineDtoConverter.toDto(it)
                                notificationService.notify("/order_lines/update", result)
                                result
                            })

    override fun update(id: Long,
                        orderLineDto: OrderLineDto) =
            useCaseExecutor(useCase = orderLineSaveUseCase,
                            requestDto = orderLineDto,
                            requestConverter = {
                                it.id = id
                                OrderLineDtoConverter.toEntity(it)
                            },
                            responseConverter = {
                                val result = OrderLineDtoConverter.toDto(it)
                                notificationService.notify("/order_lines/update", result)
                                result
                            })

    override fun delete(id: Long) =
            useCaseExecutor(useCase = orderLineDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}