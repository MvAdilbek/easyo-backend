package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.MealParam
import kz.easy_o.delivery.dto.MealDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/meals")
interface MealResource {

    @GetMapping
    fun page(@ModelAttribute mealParam: MealParam): CompletionStage<Page<MealDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<MealDto>

    @PostMapping
    fun save(@RequestBody mealDto: MealDto): CompletionStage<MealDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody mealDto: MealDto): CompletionStage<MealDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}