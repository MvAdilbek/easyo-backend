package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.MenuParam
import kz.easy_o.delivery.dto.MenuDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/menus")
interface MenuResource {

    @GetMapping
    fun page(@ModelAttribute menuParam: MenuParam): CompletionStage<Page<MenuDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<MenuDto>

    @PostMapping
    fun save(@RequestBody menuDto: MenuDto): CompletionStage<MenuDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody menuDto: MenuDto): CompletionStage<MenuDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}