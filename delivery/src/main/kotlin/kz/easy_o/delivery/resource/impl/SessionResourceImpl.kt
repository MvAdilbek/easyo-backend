package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.SessionParam
import kz.easy_o.core.parameter.session.SessionSaveParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.converter.SessionDtoConverter
import kz.easy_o.delivery.resource.SessionResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.session.SessionByIdUseCase
import kz.easy_o.usecases.module.session.SessionDeleteUseCase
import kz.easy_o.usecases.module.session.SessionPageUseCase
import kz.easy_o.usecases.module.session.SessionSaveUseCase

class SessionResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val sessionPageUseCase: SessionPageUseCase,
        private val sessionByIdUseCase: SessionByIdUseCase,
        private val sessionSaveUseCase: SessionSaveUseCase,
        private val sessionDeleteUseCase: SessionDeleteUseCase
) : SessionResource {

    override fun page(sessionParam: SessionParam) =
            useCaseExecutor(useCase = sessionPageUseCase,
                            requestDto = sessionParam,
                            requestConverter = { it },
                            responseConverter = { it.map(SessionDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = sessionByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { SessionDtoConverter.toDto(it) })

    override fun create(sessionSaveParam: SessionSaveParam) =
            useCaseExecutor(useCase = sessionSaveUseCase,
                            requestDto = sessionSaveParam,
                            requestConverter = { it },
                            responseConverter = { SessionDtoConverter.toDto(it) })

    override fun delete(id: Long) =
            useCaseExecutor(useCase = sessionDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}