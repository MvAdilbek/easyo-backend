package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.MealParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.MealDto
import kz.easy_o.delivery.dto.converter.MealDtoConverter
import kz.easy_o.delivery.resource.MealResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.meal.MealByIdUseCase
import kz.easy_o.usecases.module.meal.MealDeleteUseCase
import kz.easy_o.usecases.module.meal.MealPageUseCase
import kz.easy_o.usecases.module.meal.MealSaveUseCase

class MealResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val mealPageUseCase: MealPageUseCase,
        private val mealByIdUseCase: MealByIdUseCase,
        private val mealSaveUseCase: MealSaveUseCase,
        private val mealDeleteUseCase: MealDeleteUseCase
) : MealResource {

    override fun page(mealParam: MealParam) =
            useCaseExecutor(useCase = mealPageUseCase,
                            requestDto = mealParam,
                            requestConverter = { it },
                            responseConverter = { it.map(MealDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = mealByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = MealDtoConverter::toDto)

    override fun save(mealDto: MealDto) =
            useCaseExecutor(useCase = mealSaveUseCase,
                            requestDto = mealDto,
                            requestConverter = {
                                it.id = null
                                MealDtoConverter.toEntity(it)
                            },
                            responseConverter = MealDtoConverter::toDto)

    override fun update(id: Long,
                        mealDto: MealDto) =
            useCaseExecutor(useCase = mealSaveUseCase,
                            requestDto = mealDto,
                            requestConverter = {
                                it.id = id
                                MealDtoConverter.toEntity(it)
                            },
                            responseConverter = MealDtoConverter::toDto)

    override fun delete(id: Long) =
            useCaseExecutor(useCase = mealDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}