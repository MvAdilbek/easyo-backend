package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.BillParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.BillDto
import kz.easy_o.delivery.dto.converter.BillDtoConverter
import kz.easy_o.delivery.resource.BillResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.bill.BillByIdUseCase
import kz.easy_o.usecases.module.bill.BillDeleteUseCase
import kz.easy_o.usecases.module.bill.BillPageUseCase
import kz.easy_o.usecases.module.bill.BillSaveUseCase

class BillResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val billPageUseCase: BillPageUseCase,
        private val billByIdUseCase: BillByIdUseCase,
        private val billSaveUseCase: BillSaveUseCase,
        private val billDeleteUseCase: BillDeleteUseCase
) : BillResource {

    override fun page(billParam: BillParam) =
            useCaseExecutor(useCase = billPageUseCase,
                            requestDto = billParam,
                            requestConverter = { it },
                            responseConverter = { it.map(BillDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = billByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = BillDtoConverter::toDto)

    override fun save(billDto: BillDto) =
            useCaseExecutor(useCase = billSaveUseCase,
                            requestDto = billDto,
                            requestConverter = {
                                it.id = null
                                BillDtoConverter.toEntity(it)
                            },
                            responseConverter = BillDtoConverter::toDto)

    override fun update(id: Long, billDto: BillDto) =
            useCaseExecutor(useCase = billSaveUseCase,
                            requestDto = billDto,
                            requestConverter = {
                                it.id = id
                                BillDtoConverter.toEntity(it)
                            },
                            responseConverter = BillDtoConverter::toDto)

    override fun delete(id: Long) =
            useCaseExecutor(useCase = billDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}