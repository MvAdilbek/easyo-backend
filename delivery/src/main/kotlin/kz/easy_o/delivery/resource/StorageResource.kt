package kz.easy_o.delivery.resource

import kz.easy_o.core.StorageFileConfig
import kz.easy_o.core.constant.Constant
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/storage")
interface StorageResource {

    @PostMapping("/upload")
    fun upload(@RequestParam("file") multipartFile: MultipartFile): CompletionStage<StorageFileConfig>
}