package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.MenuParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.MenuDto
import kz.easy_o.delivery.dto.converter.MenuDtoConverter
import kz.easy_o.delivery.resource.MenuResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.menu.MenuByIdUseCase
import kz.easy_o.usecases.module.menu.MenuDeleteUseCase
import kz.easy_o.usecases.module.menu.MenuPageUseCase
import kz.easy_o.usecases.module.menu.MenuSaveUseCase

class MenuResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val menuPageUseCase: MenuPageUseCase,
        private val menuByIdUseCase: MenuByIdUseCase,
        private val menuSaveUseCase: MenuSaveUseCase,
        private val menuDeleteUseCase: MenuDeleteUseCase
) : MenuResource {

    override fun page(menuParam: MenuParam) =
            useCaseExecutor(useCase = menuPageUseCase,
                            requestDto = menuParam,
                            requestConverter = { it },
                            responseConverter = { it.map(MenuDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = menuByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = MenuDtoConverter::toDto)

    override fun save(menuDto: MenuDto) =
            useCaseExecutor(useCase = menuSaveUseCase,
                            requestDto = menuDto,
                            requestConverter = {
                                it.id = null
                                MenuDtoConverter.toEntity(it)
                            },
                            responseConverter = MenuDtoConverter::toDto)

    override fun update(id: Long,
                        menuDto: MenuDto) =
            useCaseExecutor(useCase = menuSaveUseCase,
                            requestDto = menuDto,
                            requestConverter = {
                                it.id = id
                                MenuDtoConverter.toEntity(it)
                            },
                            responseConverter = MenuDtoConverter::toDto)

    override fun delete(id: Long) =
            useCaseExecutor(useCase = menuDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}