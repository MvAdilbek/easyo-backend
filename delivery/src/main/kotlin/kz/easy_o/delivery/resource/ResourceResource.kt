package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.ResourceParam
import kz.easy_o.delivery.dto.ResourceDto
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/resources")
interface ResourceResource {

    @GetMapping
    fun page(@ModelAttribute resourceParam: ResourceParam): CompletionStage<Page<ResourceDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<ResourceDto>

    @PreAuthorize("isAdmin()")
    @PostMapping
    fun save(@RequestBody resourceDto: ResourceDto): CompletionStage<ResourceDto>

    @PreAuthorize("isAdmin()")
    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody resourceDto: ResourceDto): CompletionStage<ResourceDto>

    @PreAuthorize("isAdmin()")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}