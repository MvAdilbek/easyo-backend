package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.BillParam
import kz.easy_o.delivery.dto.BillDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/bills")
interface BillResource {

    @GetMapping
    fun page(@ModelAttribute billParam: BillParam): CompletionStage<Page<BillDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<BillDto>

    @PostMapping
    fun save(@RequestBody billDto: BillDto): CompletionStage<BillDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody billDto: BillDto): CompletionStage<BillDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}