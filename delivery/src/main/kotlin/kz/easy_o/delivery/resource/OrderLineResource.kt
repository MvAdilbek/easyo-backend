package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.OrderLineParam
import kz.easy_o.delivery.dto.OrderDto
import kz.easy_o.delivery.dto.OrderLineDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/order_lines")
interface OrderLineResource {

    @GetMapping
    fun page(@ModelAttribute orderLineParam: OrderLineParam): CompletionStage<Page<OrderLineDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<OrderLineDto>

    @PostMapping
    fun save(@RequestBody orderLineDto: OrderLineDto): CompletionStage<OrderLineDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody orderLineDto: OrderLineDto): CompletionStage<OrderLineDto>

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}