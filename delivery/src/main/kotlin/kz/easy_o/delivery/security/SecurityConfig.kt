package kz.easy_o.delivery.security

import com.fasterxml.jackson.databind.ObjectMapper
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.enums.ErrorCodeEnum
import kz.easy_o.core.error.ForbiddenError
import kz.easy_o.delivery.security.filter.AuthenticationFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Configuration
@EnableWebSecurity
class SecurityConfig @Autowired constructor(
        private val authenticationFilter: AuthenticationFilter
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun corsConfigurer(): WebMvcConfigurer {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry?) {
                registry!!.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedHeaders("*")
                        .allowedMethods("*")
            }
        }
    }

    override fun configure(http: HttpSecurity?) {
        http!!
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf()
                .disable()
                .cors()
                .and()
                .formLogin()
                .disable()
                .httpBasic()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint())
                .and()
                .authorizeRequests()
                .antMatchers("/",
                             "/error",
                             "/favicon.ico",
                             "/**/*.png",
                             "/**/*.gif",
                             "/**/*.svg",
                             "/**/*.jpg",
                             "/**/*.html",
                             "/**/*.css",
                             "/**/*.js")
                .permitAll()
                .antMatchers(Constant.CONTEXT_PATH + "/sessions/auth",
                             "/swagger-ui.html",
                             "/swagger-ui.html/**",
                             "/ws",
                             "/ws/**",
                             "/swagger-ui.html",
                             "/swagger-ui.html/**")
                .permitAll()
                .anyRequest()
                .authenticated()

        http.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    @Bean
    fun authenticationEntryPoint(): AuthenticationEntryPoint {
        return AuthenticationEntryPoint { _: HttpServletRequest?, response: HttpServletResponse, ex: AuthenticationException? ->
            val forbiddenError = ForbiddenError(code = ErrorCodeEnum.FORBIDDEN,
                                                message = ex?.message
                                                        ?: "Forbidden")

            val message = ObjectMapper().writeValueAsString(forbiddenError)

            response.contentType = MediaType.APPLICATION_JSON_VALUE
            response.status = HttpStatus.FORBIDDEN.value()
            response.writer.write(message)
        }
    }
}