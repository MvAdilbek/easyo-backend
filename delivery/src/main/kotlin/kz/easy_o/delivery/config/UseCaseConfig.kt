package kz.easy_o.delivery.config

import kz.easy_o.delivery.exception_handler.GlobalExceptionHandler
import kz.easy_o.usecases.common.UseCaseExecutorImpl
import kz.easy_o.usecases.property.AppProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UseCaseConfig {

    @Bean
    fun useCaseExecutor() =
            UseCaseExecutorImpl()

    @Bean
    fun globalExceptionHandler(appProperty: AppProperty) =
            GlobalExceptionHandler(appProperty)
}