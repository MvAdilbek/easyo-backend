package kz.easy_o.delivery.config.module

import kz.easy_o.delivery.resource.impl.StorageResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.storage.StorageUploadUseCase
import kz.easy_o.usecases.service.GoogleStorageService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class StorageModule {

    @Bean
    fun storageResource(useCaseExecutor: UseCaseExecutor,
                        storageUploadUseCase: StorageUploadUseCase) =
            StorageResourceImpl(useCaseExecutor,
                                storageUploadUseCase)
    
    @Bean
    fun storageUploadUseCase(googleStorageService: GoogleStorageService) =
            StorageUploadUseCase(googleStorageService)
}