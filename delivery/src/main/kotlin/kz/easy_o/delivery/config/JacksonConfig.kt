package kz.easy_o.delivery.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import kz.easy_o.core.constant.Constant
import kz.easy_o.delivery.serializer.BigDecimalDeserializer
import kz.easy_o.delivery.serializer.EmptyStringDeserializers
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import java.math.BigDecimal
import java.nio.charset.Charset
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Configuration
class JacksonConfig {

    @Bean
    fun objectMapper(builder: Jackson2ObjectMapperBuilder): MappingJackson2HttpMessageConverter {
        val timeModule = JavaTimeModule()
        timeModule.addDeserializer(LocalDateTime::class.java,
                                   LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_DATE_TIME_FORMAT)))
        timeModule.addDeserializer(LocalDate::class.java,
                                   LocalDateDeserializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_DATE_FORMAT)))
        timeModule.addDeserializer(LocalTime::class.java,
                                   LocalTimeDeserializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_TIME_FORMAT)))

        timeModule.addSerializer(LocalDateTime::class.java,
                                 LocalDateTimeSerializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_DATE_TIME_FORMAT)))
        timeModule.addSerializer(LocalDate::class.java,
                                 LocalDateSerializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_DATE_FORMAT)))
        timeModule.addSerializer(LocalTime::class.java,
                                 LocalTimeSerializer(DateTimeFormatter.ofPattern(Constant.Date.JACKSON_TIME_FORMAT)))

        val objectMapper = ObjectMapper()
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .registerModule(timeModule)
                .registerModule(ParameterNamesModule())
                .registerModule(SimpleModule().addDeserializer(String::class.java,
                                                               EmptyStringDeserializers.EMPTY_STRING_SERIALIZER_INSTANCE))
                .registerModule(SimpleModule().addDeserializer(BigDecimal::class.java,
                                                               BigDecimalDeserializer.BIG_DECIMAL_MONEY_DESERIALIZER))
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true)
                .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)

        val mappingJackson2HttpMessageConverter = MappingJackson2HttpMessageConverter()

        mappingJackson2HttpMessageConverter.objectMapper = objectMapper
        mappingJackson2HttpMessageConverter.defaultCharset = Charset.forName("UTF-8")

        return mappingJackson2HttpMessageConverter
    }
}