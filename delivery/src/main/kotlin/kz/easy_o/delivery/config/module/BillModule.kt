package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaBillDbRepository
import kz.easy_o.dataproviders.repository.impl.BillRepositoryImpl
import kz.easy_o.delivery.resource.impl.BillResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.bill.BillByIdUseCase
import kz.easy_o.usecases.module.bill.BillDeleteUseCase
import kz.easy_o.usecases.module.bill.BillPageUseCase
import kz.easy_o.usecases.module.bill.BillSaveUseCase
import kz.easy_o.usecases.validator.`object`.BillEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BillModule {

    @Bean
    fun billResource(useCaseExecutor: UseCaseExecutor,
                     billPageUseCase: BillPageUseCase,
                     billByIdUseCase: BillByIdUseCase,
                     billSaveUseCase: BillSaveUseCase,
                     billDeleteUseCase: BillDeleteUseCase) =
            BillResourceImpl(useCaseExecutor,
                             billPageUseCase,
                             billByIdUseCase,
                             billSaveUseCase,
                             billDeleteUseCase)

    @Bean
    fun billByIdUseCase(billRepository: BillByIdUseCase.BillRepository) =
            BillByIdUseCase(billRepository)

    @Bean
    fun billDeleteUseCase(billRepository: BillDeleteUseCase.BillRepository) =
            BillDeleteUseCase(billRepository)

    @Bean
    fun billPageUseCase(billRepository: BillPageUseCase.BillRepository) =
            BillPageUseCase(billRepository)

    @Bean
    fun billSaveUseCase(billEntityValidator: BillEntityValidator,
                        billRepository: BillSaveUseCase.BillRepository) =
            BillSaveUseCase(billEntityValidator,
                            billRepository)

    @Bean
    fun billEntityValidator() =
            BillEntityValidator()

    @Bean
    fun billRepository(jpaQueryFactory: JPAQueryFactory,
                       jpaBillDbRepository: JpaBillDbRepository) =
            BillRepositoryImpl(jpaQueryFactory,
                               jpaBillDbRepository)
}