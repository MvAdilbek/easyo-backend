package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaSessionDbRepository
import kz.easy_o.dataproviders.repository.impl.SessionRepositoryImpl
import kz.easy_o.delivery.resource.impl.SessionResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.session.*
import kz.easy_o.usecases.property.AppProperty
import kz.easy_o.usecases.service.GenerateTextService
import kz.easy_o.usecases.validator.`object`.SessionSaveParamValidator
import kz.easy_o.usecases.validator.`object`.UserEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SessionModule {

    @Bean
    fun sessionResourceImpl(useCaseExecutor: UseCaseExecutor,
                            sessionPageUseCase: SessionPageUseCase,
                            sessionByIdUseCase: SessionByIdUseCase,
                            sessionSaveUseCase: SessionSaveUseCase,
                            sessionDeleteUseCase: SessionDeleteUseCase) =
            SessionResourceImpl(useCaseExecutor,
                                sessionPageUseCase,
                                sessionByIdUseCase,
                                sessionSaveUseCase,
                                sessionDeleteUseCase)

    @Bean
    fun sessionSocketRegisterUseCase(sessionRepository: SessionSocketRegisterUseCase.SessionRepository) =
            SessionSocketRegisterUseCase(sessionRepository)

    @Bean
    fun sessionByIdUseCase(sessionRepository: SessionByIdUseCase.SessionRepository) =
            SessionByIdUseCase(sessionRepository)

    @Bean
    fun sessionPageUseCase(sessionRepository: SessionPageUseCase.SessionRepository) =
            SessionPageUseCase(sessionRepository)

    @Bean
    fun sessionAuthUseCase(sessionRepository: SessionAuthUseCase.SessionRepository) =
            SessionAuthUseCase(sessionRepository)

    @Bean
    fun sessionSaveUseCase(sessionSaveParamValidator: SessionSaveParamValidator,
                           sessionRepository: SessionSaveUseCase.SessionRepository,
                           userRepository: SessionSaveUseCase.UserRepository,
                           generateTextService: GenerateTextService) =
            SessionSaveUseCase(sessionSaveParamValidator,
                               sessionRepository,
                               userRepository,
                               generateTextService)

    @Bean
    fun sessionDeleteUseCase(sessionRepository: SessionDeleteUseCase.SessionRepository) =
            SessionDeleteUseCase(sessionRepository)

    @Bean
    fun sessionSaveParamValidator() =
            SessionSaveParamValidator()

    @Bean
    fun sessionRepository(jpaQueryFactory: JPAQueryFactory,
                          jpaSessionDbRepository: JpaSessionDbRepository) =
            SessionRepositoryImpl(jpaQueryFactory,
                                  jpaSessionDbRepository)
}