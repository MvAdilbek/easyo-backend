package kz.easy_o.delivery.config

import com.fasterxml.jackson.databind.ObjectMapper
import kz.easy_o.core.parameter.SessionSocketRegisterParam
import kz.easy_o.usecases.common.authorizedUserPrincipal
import kz.easy_o.usecases.common.isAuthenticated
import kz.easy_o.usecases.module.session.SessionSocketRegisterUseCase
import kz.easy_o.usecases.service.impl.NotificationServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpRequest
import org.springframework.messaging.converter.MessageConverter
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.messaging.simp.config.ChannelRegistration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.server.HandshakeInterceptor
import org.springframework.web.socket.server.support.DefaultHandshakeHandler
import java.security.Principal

@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig(
        @Autowired val sessionSocketRegisterUseCase: SessionSocketRegisterUseCase
) : WebSocketMessageBrokerConfigurer {

    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker("/topic",
                                  "/queue",
                                  "/orders",
                                  "/order_lines")
        config.setApplicationDestinationPrefixes("/app")
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/ws")
                .setAllowedOrigins("*")
                .setHandshakeHandler(object : DefaultHandshakeHandler() {
                    override fun determineUser(request: ServerHttpRequest,
                                               wsHandler: WebSocketHandler,
                                               attributes: MutableMap<String, Any>): Principal {
                        val sessionId = attributes["sessionId"] as String
                        return StompPrincipal(sessionId)
                    }
                })
                .withSockJS()
                .setInterceptors(HttpHandshakeInterceptor(sessionSocketRegisterUseCase))
    }

    override fun configureClientInboundChannel(registration: ChannelRegistration) {
    }

    override fun configureClientOutboundChannel(registration: ChannelRegistration) {
    }

    override fun configureMessageConverters(messageConverters: MutableList<MessageConverter>): Boolean {
        return true
    }

    @Bean
    fun notificationService(simpMessagingTemplate: SimpMessagingTemplate,
                            objectMapper: ObjectMapper) =
            NotificationServiceImpl(simpMessagingTemplate,
                                    objectMapper)

    class HttpHandshakeInterceptor(
            private val sessionSocketRegisterUseCase: SessionSocketRegisterUseCase
    ) : HandshakeInterceptor {
        override fun afterHandshake(request: ServerHttpRequest,
                                    response: ServerHttpResponse,
                                    handler: WebSocketHandler,
                                    exception: Exception?) {
        }

        override fun beforeHandshake(request: ServerHttpRequest,
                                     response: ServerHttpResponse,
                                     handler: WebSocketHandler,
                                     attributes: MutableMap<String, Any>): Boolean {
            if (request is ServletServerHttpRequest && isAuthenticated) {
                val socketSessionId = request.servletRequest.session?.id!!
                val sessionId = authorizedUserPrincipal?.sessionEntity?.id!!
                attributes["sessionId"] = socketSessionId
                sessionSocketRegisterUseCase.execute(
                        SessionSocketRegisterParam(sessionId = sessionId,
                                                   socketSessionId = socketSessionId)
                )
            }
            return true
        }
    }

    class StompPrincipal(private val sessionId: String) : Principal {
        override fun getName(): String {
            return sessionId
        }
    }
}