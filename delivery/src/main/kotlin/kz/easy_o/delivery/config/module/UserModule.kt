package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaUserDbRepository
import kz.easy_o.dataproviders.repository.impl.UserRepositoryImpl
import kz.easy_o.delivery.resource.impl.UserResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.user.*
import kz.easy_o.usecases.service.MailSenderService
import kz.easy_o.usecases.validator.`object`.UserEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UserModule {

    @Bean
    fun userResource(useCaseExecutor: UseCaseExecutor,
                     userPageUseCase: UserPageUseCase,
                     userByIdUseCase: UserByIdUseCase,
                     userSaveUseCase: UserSaveUseCase,
                     userDeleteUseCase: UserDeleteUseCase) =
            UserResourceImpl(useCaseExecutor,
                             userPageUseCase,
                             userByIdUseCase,
                             userSaveUseCase,
                             userDeleteUseCase)

    @Bean
    fun userDeleteUseCase(userRepository: UserDeleteUseCase.UserRepository) =
            UserDeleteUseCase(userRepository)

    @Bean
    fun userSaveUseCase(userEntityValidator: UserEntityValidator,
                        userRepository: UserSaveUseCase.UserRepository,
                        mailSenderService: MailSenderService) =
            UserSaveUseCase(userEntityValidator,
                            userRepository,
                            mailSenderService)

    @Bean
    fun userByIdUseCase(userRepository: UserByIdUseCase.UserRepository) =
            UserByIdUseCase(userRepository)

    @Bean
    fun userPageUseCase(userRepository: UserPageUseCase.UserRepository) =
            UserPageUseCase(userRepository)

    @Bean
    fun userEntityValidator() =
            UserEntityValidator()

    @Bean
    fun userRepository(jpaQueryFactory: JPAQueryFactory,
                       jpaUserDbRepository: JpaUserDbRepository) =
            UserRepositoryImpl(jpaQueryFactory, jpaUserDbRepository)
}