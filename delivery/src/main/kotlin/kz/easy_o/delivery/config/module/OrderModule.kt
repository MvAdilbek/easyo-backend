package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaOrderDbRepository
import kz.easy_o.dataproviders.repository.JpaOrderLineDbRepository
import kz.easy_o.dataproviders.repository.impl.OrderRepositoryImpl
import kz.easy_o.delivery.resource.impl.OrderResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.order.OrderByIdUseCase
import kz.easy_o.usecases.module.order.OrderDeleteUseCase
import kz.easy_o.usecases.module.order.OrderPageUseCase
import kz.easy_o.usecases.module.order.OrderSaveUseCase
import kz.easy_o.usecases.service.NotificationService
import kz.easy_o.usecases.validator.`object`.OrderEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OrderModule {

    @Bean
    fun orderResource(useCaseExecutor: UseCaseExecutor,
                      orderPageUseCase: OrderPageUseCase,
                      orderByIdUseCase: OrderByIdUseCase,
                      orderSaveUseCase: OrderSaveUseCase,
                      orderDeleteUseCase: OrderDeleteUseCase,
                      notificationService: NotificationService) =
            OrderResourceImpl(useCaseExecutor,
                              orderPageUseCase,
                              orderByIdUseCase,
                              orderSaveUseCase,
                              orderDeleteUseCase,
                              notificationService)

    @Bean
    fun orderByIdUseCase(orderRepository: OrderByIdUseCase.OrderRepository) =
            OrderByIdUseCase(orderRepository)

    @Bean
    fun orderDeleteUseCase(orderRepository: OrderDeleteUseCase.OrderRepository) =
            OrderDeleteUseCase(orderRepository)

    @Bean
    fun orderPageUseCase(orderRepository: OrderPageUseCase.OrderRepository) =
            OrderPageUseCase(orderRepository)

    @Bean
    fun orderSaveUseCase(orderEntityValidator: OrderEntityValidator,
                         orderRepository: OrderSaveUseCase.OrderRepository) =
            OrderSaveUseCase(orderEntityValidator,
                             orderRepository)

    @Bean
    fun orderEntityValidator() =
            OrderEntityValidator()

    @Bean
    fun orderRepository(jpaQueryFactory: JPAQueryFactory,
                        jpaOrderDbRepository: JpaOrderDbRepository,
                        jpaOrderLineDbRepository: JpaOrderLineDbRepository) =
            OrderRepositoryImpl(jpaQueryFactory,
                                jpaOrderDbRepository,
                                jpaOrderLineDbRepository)
}