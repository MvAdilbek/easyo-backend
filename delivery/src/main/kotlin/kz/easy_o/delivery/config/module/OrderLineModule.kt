package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaOrderLineDbRepository
import kz.easy_o.dataproviders.repository.impl.OrderLineRepositoryImpl
import kz.easy_o.delivery.resource.impl.OrderLineResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.order_line.OrderLineByIdUseCase
import kz.easy_o.usecases.module.order_line.OrderLineDeleteUseCase
import kz.easy_o.usecases.module.order_line.OrderLinePageUseCase
import kz.easy_o.usecases.module.order_line.OrderLineSaveUseCase
import kz.easy_o.usecases.service.NotificationService
import kz.easy_o.usecases.validator.`object`.OrderLineEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OrderLineModule {

    @Bean
    fun orderLineResource(useCaseExecutor: UseCaseExecutor,
                          orderLinePageUseCase: OrderLinePageUseCase,
                          orderLineByIdUseCase: OrderLineByIdUseCase,
                          orderLineSaveUseCase: OrderLineSaveUseCase,
                          orderLineDeleteUseCase: OrderLineDeleteUseCase,
                          notificationService: NotificationService) =
            OrderLineResourceImpl(useCaseExecutor,
                                  orderLinePageUseCase,
                                  orderLineByIdUseCase,
                                  orderLineSaveUseCase,
                                  orderLineDeleteUseCase,
                                  notificationService)

    @Bean
    fun orderLineByIdUseCase(orderLineRepository: OrderLineByIdUseCase.OrderLineRepository) =
            OrderLineByIdUseCase(orderLineRepository)

    @Bean
    fun orderLineDeleteUseCase(orderLineRepository: OrderLineDeleteUseCase.OrderLineRepository) =
            OrderLineDeleteUseCase(orderLineRepository)

    @Bean
    fun orderLinePageUseCase(orderLineRepository: OrderLinePageUseCase.OrderLineRepository) =
            OrderLinePageUseCase(orderLineRepository)

    @Bean
    fun orderLineSaveUseCase(orderLineEntityValidator: OrderLineEntityValidator,
                             orderLineRepository: OrderLineSaveUseCase.OrderLineRepository) =
            OrderLineSaveUseCase(orderLineEntityValidator,
                                 orderLineRepository)

    @Bean
    fun orderLineEntityValidator() =
            OrderLineEntityValidator()

    @Bean
    fun orderLineRepository(jpaQueryFactory: JPAQueryFactory,
                            jpaOrderLineDbRepository: JpaOrderLineDbRepository) =
            OrderLineRepositoryImpl(jpaQueryFactory, jpaOrderLineDbRepository)
}