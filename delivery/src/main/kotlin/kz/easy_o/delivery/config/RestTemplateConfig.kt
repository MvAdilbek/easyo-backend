package kz.easy_o.delivery.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.DefaultUriBuilderFactory

@Configuration
class RestTemplateConfig {

    @Bean
    fun getRestTemplate(): RestTemplate? {
        val restTemplate = RestTemplate()
        val defaultUriBuilderFactory = DefaultUriBuilderFactory()
        defaultUriBuilderFactory.encodingMode = DefaultUriBuilderFactory.EncodingMode.NONE
        restTemplate.uriTemplateHandler = defaultUriBuilderFactory
        return restTemplate
    }
}