package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaTableDbRepository
import kz.easy_o.dataproviders.repository.impl.TableRepositoryImpl
import kz.easy_o.delivery.resource.impl.TableResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.table.TableByIdUseCase
import kz.easy_o.usecases.module.table.TableDeleteUseCase
import kz.easy_o.usecases.module.table.TablePageUseCase
import kz.easy_o.usecases.module.table.TableSaveUseCase
import kz.easy_o.usecases.validator.`object`.TableEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TableModule {

    @Bean
    fun tableResource(useCaseExecutor: UseCaseExecutor,
                      tablePageUseCase: TablePageUseCase,
                      tableByIdUseCase: TableByIdUseCase,
                      tableSaveUseCase: TableSaveUseCase,
                      tableDeleteUseCase: TableDeleteUseCase) =
            TableResourceImpl(useCaseExecutor,
                              tablePageUseCase,
                              tableByIdUseCase,
                              tableSaveUseCase,
                              tableDeleteUseCase)

    @Bean
    fun tableByIdUseCase(tableRepository: TableByIdUseCase.TableRepository) =
            TableByIdUseCase(tableRepository)

    @Bean
    fun tableDeleteUseCase(tableRepository: TableDeleteUseCase.TableRepository) =
            TableDeleteUseCase(tableRepository)

    @Bean
    fun tablePageUseCase(tableRepository: TablePageUseCase.TableRepository) =
            TablePageUseCase(tableRepository)

    @Bean
    fun tableSaveUseCase(tableEntityValidator: TableEntityValidator,
                         tableRepository: TableSaveUseCase.TableRepository) =
            TableSaveUseCase(tableEntityValidator,
                             tableRepository)

    @Bean
    fun tableEntityValidator() =
            TableEntityValidator()

    @Bean
    fun tableRepository(jpaQueryFactory: JPAQueryFactory,
                        jpaTableDbRepository: JpaTableDbRepository) =
            TableRepositoryImpl(jpaQueryFactory,
                                jpaTableDbRepository)
}