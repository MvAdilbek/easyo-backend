package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaMenuDbRepository
import kz.easy_o.dataproviders.repository.impl.MenuRepositoryImpl
import kz.easy_o.delivery.resource.impl.MenuResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.menu.MenuByIdUseCase
import kz.easy_o.usecases.module.menu.MenuDeleteUseCase
import kz.easy_o.usecases.module.menu.MenuPageUseCase
import kz.easy_o.usecases.module.menu.MenuSaveUseCase
import kz.easy_o.usecases.validator.`object`.MenuEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MenuModule {

    @Bean
    fun menuResource(useCaseExecutor: UseCaseExecutor,
                     menuPageUseCase: MenuPageUseCase,
                     menuByIdUseCase: MenuByIdUseCase,
                     menuSaveUseCase: MenuSaveUseCase,
                     menuDeleteUseCase: MenuDeleteUseCase) =
            MenuResourceImpl(useCaseExecutor,
                             menuPageUseCase,
                             menuByIdUseCase,
                             menuSaveUseCase,
                             menuDeleteUseCase)

    @Bean
    fun menuByIdUseCase(menuRepository: MenuByIdUseCase.MenuRepository) =
            MenuByIdUseCase(menuRepository)

    @Bean
    fun menuDeleteUseCase(menuRepository: MenuDeleteUseCase.MenuRepository) =
            MenuDeleteUseCase(menuRepository)

    @Bean
    fun menuPageUseCase(menuRepository: MenuPageUseCase.MenuRepository) =
            MenuPageUseCase(menuRepository)

    @Bean
    fun menuSaveUseCase(menuEntityValidator: MenuEntityValidator,
                        menuRepository: MenuSaveUseCase.MenuRepository) =
            MenuSaveUseCase(menuEntityValidator,
                            menuRepository)

    @Bean
    fun menuEntityValidator() =
            MenuEntityValidator()

    @Bean
    fun menuRepository(jpaQueryFactory: JPAQueryFactory,
                       jpaMenuDbRepository: JpaMenuDbRepository) =
            MenuRepositoryImpl(jpaQueryFactory,
                               jpaMenuDbRepository)
}