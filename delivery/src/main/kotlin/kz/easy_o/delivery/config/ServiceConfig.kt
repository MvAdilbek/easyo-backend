package kz.easy_o.delivery.config

import com.google.cloud.storage.Storage
import kz.easy_o.usecases.property.AppProperty
import kz.easy_o.usecases.service.impl.GenerateTextServiceImpl
import kz.easy_o.usecases.service.impl.GoogleStorageServiceImpl
import kz.easy_o.usecases.service.impl.MailSenderServiceImpl
import kz.easy_o.usecases.service.impl.NetworkServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.web.client.RestTemplate

@Configuration
class ServiceConfig {

    @Bean
    fun generateTextServiceImpl() =
            GenerateTextServiceImpl()

    @Bean
    fun networkService(restTemplate: RestTemplate) =
            NetworkServiceImpl(restTemplate)

    @Bean
    fun mailSenderService(javaMailSender: JavaMailSender,
                          appProperty: AppProperty) =
            MailSenderServiceImpl(javaMailSender,
                                  appProperty)

    @Bean
    fun googleStorageService(storage: Storage) =
            GoogleStorageServiceImpl(storage)
}