package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaMealDbRepository
import kz.easy_o.dataproviders.repository.JpaMenuMealDbRepository
import kz.easy_o.dataproviders.repository.impl.MealRepositoryImpl
import kz.easy_o.delivery.resource.impl.MealResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.meal.MealByIdUseCase
import kz.easy_o.usecases.module.meal.MealDeleteUseCase
import kz.easy_o.usecases.module.meal.MealPageUseCase
import kz.easy_o.usecases.module.meal.MealSaveUseCase
import kz.easy_o.usecases.validator.`object`.MealEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MealModule {

    @Bean
    fun mealResource(useCaseExecutor: UseCaseExecutor,
                     mealPageUseCase: MealPageUseCase,
                     mealByIdUseCase: MealByIdUseCase,
                     mealSaveUseCase: MealSaveUseCase,
                     mealDeleteUseCase: MealDeleteUseCase) =
            MealResourceImpl(useCaseExecutor,
                             mealPageUseCase,
                             mealByIdUseCase,
                             mealSaveUseCase,
                             mealDeleteUseCase)

    @Bean
    fun mealByIdUseCase(mealRepository: MealByIdUseCase.MealRepository) =
            MealByIdUseCase(mealRepository)

    @Bean
    fun mealDeleteUseCase(mealRepository: MealDeleteUseCase.MealRepository) =
            MealDeleteUseCase(mealRepository)

    @Bean
    fun mealPageUseCase(mealRepository: MealPageUseCase.MealRepository) =
            MealPageUseCase(mealRepository)

    @Bean
    fun mealSaveUseCase(mealEntityValidator: MealEntityValidator,
                        mealRepository: MealSaveUseCase.MealRepository) =
            MealSaveUseCase(mealEntityValidator,
                            mealRepository)

    @Bean
    fun mealEntityValidator() =
            MealEntityValidator()

    @Bean
    fun mealRepositoryImpl(jpaQueryFactory: JPAQueryFactory,
                           jpaMealDbRepository: JpaMealDbRepository,
                           jpaMenuMealDbRepository: JpaMenuMealDbRepository) =
            MealRepositoryImpl(jpaQueryFactory,
                               jpaMealDbRepository,
                               jpaMenuMealDbRepository)
}