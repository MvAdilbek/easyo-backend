package kz.easy_o.delivery.exception_handler

import kz.easy_o.core.enums.ErrorCodeEnum
import kz.easy_o.core.error.FieldError
import kz.easy_o.core.error.ForbiddenError
import kz.easy_o.core.error.SimpleError
import kz.easy_o.core.error.ValidationError
import kz.easy_o.core.error.intf.Error
import kz.easy_o.usecases.exception.FieldsException
import kz.easy_o.usecases.exception.NotFoundException
import kz.easy_o.usecases.property.AppProperty
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.AuthenticationException
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.NoHandlerFoundException

@RestControllerAdvice
class GlobalExceptionHandler(
        private val appProperty: AppProperty
) {
    @ExceptionHandler(Exception::class, RuntimeException::class)
    fun runtimeException(ex: Exception): ResponseEntity<Error> {
        if (appProperty.profile != "prod")
            throw ex

        val error = SimpleError(code = ErrorCodeEnum.INTERNAL_SERVER_ERROR,
                                message = ex.message ?: "No message")
        return ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(FieldsException::class)
    fun fieldsException(ex: FieldsException): ResponseEntity<Error> {
        val fieldErrorList = ex.fieldErrorList.map { FieldError(it.field, it.code ?: "No message") }

        val validationError = ValidationError(message = "Validator exception",
                                              fields = fieldErrorList)

        return ResponseEntity(validationError,
                              HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(AuthenticationException::class)
    fun authenticationException(ex: AuthenticationException): ResponseEntity<Error> {
        val forbiddenError = ForbiddenError(code = ErrorCodeEnum.FORBIDDEN,
                                            message = ex.message ?: "Forbidden")
        return ResponseEntity(forbiddenError,
                              HttpStatus.FORBIDDEN)
    }

    @ExceptionHandler(NotFoundException::class, EmptyResultDataAccessException::class)
    fun notFoundException(ex: NotFoundException): ResponseEntity<Error> {
        val error = SimpleError(code = ErrorCodeEnum.NOT_FOUND,
                                message = ex.message ?: "Not found")
        return ResponseEntity(error,
                              HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(AccessDeniedException::class)
    fun accessDeniedException(ex: AccessDeniedException): ResponseEntity<Error> {
        val error = SimpleError(code = ErrorCodeEnum.FORBIDDEN,
                                message = ex.message ?: "No access")
        return ResponseEntity(error,
                              HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(BindException::class)
    fun bindException(ex: BindException): ResponseEntity<Error> {
        val error = SimpleError(code = ErrorCodeEnum.VALIDATION,
                                message = ex.message ?: "Required field mismatch exception")
        return ResponseEntity(error,
                              HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(NoHandlerFoundException::class)
    fun resourceNotFoundException(ex: NoHandlerFoundException): ResponseEntity<Error> {
        val error = SimpleError(code = ErrorCodeEnum.NO_RESOURCE,
                                message = ex.message ?: "No resource")
        return ResponseEntity(error,
                              HttpStatus.NOT_FOUND)
    }
}