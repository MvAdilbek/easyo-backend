package kz.easy_o.delivery.common

import kz.easy_o.core.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

fun <E, T> Page<E>.map(converter: (E) -> T): Page<T> {
    val mappedData = this.data.map(converter)
    return Page(data = mappedData,
                total = this.total)
}

fun noContentResponse() =
        ResponseEntity<Unit>(HttpStatus.NO_CONTENT)