package kz.easy_o.delivery

import kz.easy_o.usecases.property.ApiSmsServiceProperty
import kz.easy_o.usecases.property.AppProperty
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication(scanBasePackages = [
    "kz.easy_o.dataproviders.config",
    "kz.easy_o.delivery.config",
    "kz.easy_o.delivery.security"
])
@EnableConfigurationProperties(ApiSmsServiceProperty::class,
                               AppProperty::class)
class App : SpringBootServletInitializer()

fun main(args: Array<String>) {
    SpringApplication.run(App::class.java, *args)
}