package kz.easy_o.delivery.serializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers
import java.math.BigDecimal
import java.math.RoundingMode

class BigDecimalDeserializer : JsonDeserializer<BigDecimal?>() {

    companion object {
        val BIG_DECIMAL_MONEY_DESERIALIZER: JsonDeserializer<BigDecimal?> = NumberDeserializers.BigDecimalDeserializer()
    }

    private val delegate = NumberDeserializers.BigDecimalDeserializer.instance

    override fun deserialize(jp: JsonParser?, ctxt: DeserializationContext?): BigDecimal {
        var bd = delegate.deserialize(jp, ctxt)
        bd = bd.setScale(2, RoundingMode.HALF_EVEN)
        return bd
    }
}