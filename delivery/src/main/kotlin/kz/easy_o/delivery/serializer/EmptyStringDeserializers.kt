package kz.easy_o.delivery.serializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException

class EmptyStringDeserializers : JsonDeserializer<String?>() {

    companion object {
        val EMPTY_STRING_SERIALIZER_INSTANCE: JsonDeserializer<String?> = EmptyStringDeserializers()
    }

    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext): String? {
        val text = jsonParser.text
        if (text.isEmpty())
            return null
        return text
    }
}