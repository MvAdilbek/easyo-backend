package kz.easy_o.dataproviders.dialect

import org.hibernate.dialect.PostgreSQL82Dialect
import org.hibernate.dialect.function.StandardSQLFunction
import org.hibernate.type.DoubleType
import org.hibernate.type.ObjectType

final class PgDialect : PostgreSQL82Dialect() {

    init {
        registerFunction("ts_rank", StandardSQLFunction("ts_rank", DoubleType.INSTANCE))
        registerFunction("to_tsquery", StandardSQLFunction("to_tsquery", ObjectType.INSTANCE))
    }
}