package kz.easy_o.dataproviders.common

import java.util.*

fun <T> Optional<T>.unwrap(): T? = orElse(null)

fun <T, E> Optional<T>.unwrap(converter: (T) -> (E)): E? = unwrap()?.let { converter(it) }