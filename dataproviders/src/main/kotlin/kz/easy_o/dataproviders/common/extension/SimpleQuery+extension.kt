package kz.easy_o.dataproviders.common.extension

import com.querydsl.jpa.impl.JPAQuery
import kz.easy_o.core.parameter.PaginationParam

fun <Q> JPAQuery<Q>.page(paginationParam: PaginationParam): JPAQuery<Q> {
    var query = this

    if (paginationParam.size != null) {
        query = query.limit(paginationParam.size!!.toLong())

        if (paginationParam.page != null)
            query = query.offset((paginationParam.size!! * paginationParam.page!!).toLong())
    }

    return query
}