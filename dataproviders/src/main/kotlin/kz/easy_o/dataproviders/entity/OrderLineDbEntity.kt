package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.core.enums.OrderLineStatusEnum
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import java.math.BigDecimal
import javax.persistence.*

@Entity
@QueryEntity
@Table(name = "order_lines")
@Loader(namedQuery = "findOrderLineDbById")
@NamedQuery(name = "findOrderLineDbById",
            query = "SELECT ent FROM OrderLineDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE order_lines SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class OrderLineDbEntity : BaseDbEntity() {
    @Column(name = "quantity")
    var quantity: Int? = null

    @Column(name = "unit_price")
    var unitPrice: BigDecimal? = null

    @Column(name = "meal_id")
    var mealId: Long? = null

    @Column(name = "order_id")
    var orderId: Long? = null

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    var orderLineStatusEnum: OrderLineStatusEnum? = null

    @Column(name = "accepted_user_id")
    var acceptedUserId: Long? = null
}