package kz.easy_o.dataproviders.entity.base

import java.time.LocalDateTime
import java.time.ZoneId
import javax.persistence.*

@MappedSuperclass
open class BaseDbEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    open var id: Long? = null

    @Column(name = "deleted")
    open var deleted: Long? = 0

    @PreRemove
    fun preDelete() {
        this.deleted = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseDbEntity

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

    override fun toString(): String {
        return id.toString()
    }
}