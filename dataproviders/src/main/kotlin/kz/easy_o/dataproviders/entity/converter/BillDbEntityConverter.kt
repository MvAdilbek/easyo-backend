package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.BillEntity
import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.dataproviders.entity.BillDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object BillDbEntityConverter : DbEntityConverter<BillDbEntity, BillEntity> {

    override fun toDbEntity(entity: BillEntity): BillDbEntity {
        val billDbEntity = BillDbEntity()

        billDbEntity.id = entity.id
        billDbEntity.billTime = entity.billTime
        billDbEntity.amount = entity.amount
        billDbEntity.orderId = entity.orderEntity?.id
        billDbEntity.acceptedUserId = entity.acceptedUserEntity?.id

        return billDbEntity
    }

    override fun toEntity(dbEntity: BillDbEntity, isView: Boolean): BillEntity {
        val billEntity = BillEntity()

        billEntity.id = dbEntity.id
        billEntity.billTime = dbEntity.billTime
        billEntity.amount = dbEntity.amount
        billEntity.orderEntity = OrderEntity(id = dbEntity.orderId)
        billEntity.acceptedUserEntity = UserEntity(id = dbEntity.acceptedUserId)

        return billEntity
    }
}