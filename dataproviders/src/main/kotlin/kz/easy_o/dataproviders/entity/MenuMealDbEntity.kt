package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.NamedQuery
import javax.persistence.Table

@Entity
@QueryEntity
@Table(name = "menus_meals")
@Loader(namedQuery = "findMenusMealsDbById")
@NamedQuery(name = "findMenusMealsDbById",
            query = "SELECT ent FROM MenuMealDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE menus_meals SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class MenuMealDbEntity : BaseDbEntity() {
    @Column(name = "menu_id")
    var menuId: Long? = null

    @Column(name = "meal_id")
    var mealId: Long? = null
}