package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.dataproviders.entity.OrderLineDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object OrderLineDbEntityConverter : DbEntityConverter<OrderLineDbEntity, OrderLineEntity> {

    override fun toDbEntity(entity: OrderLineEntity): OrderLineDbEntity {
        val orderLineDbEntity = OrderLineDbEntity()

        orderLineDbEntity.id = entity.id
        orderLineDbEntity.quantity = entity.quantity
        orderLineDbEntity.unitPrice = entity.unitPrice
        orderLineDbEntity.mealId = entity.mealEntity?.id
        orderLineDbEntity.orderId = entity.orderEntity?.id
        orderLineDbEntity.orderLineStatusEnum = entity.status
        orderLineDbEntity.acceptedUserId = entity.acceptedUserEntity?.id

        return orderLineDbEntity
    }

    override fun toEntity(dbEntity: OrderLineDbEntity, isView: Boolean): OrderLineEntity {
        val orderLineEntity = OrderLineEntity()

        orderLineEntity.id = dbEntity.id
        orderLineEntity.deleted = dbEntity.deleted
        orderLineEntity.quantity = dbEntity.quantity
        orderLineEntity.unitPrice = dbEntity.unitPrice
        orderLineEntity.mealEntity = MealEntity(id = dbEntity.mealId)
        orderLineEntity.orderEntity = OrderEntity(id = dbEntity.orderId)
        orderLineEntity.status = dbEntity.orderLineStatusEnum
        orderLineEntity.acceptedUserEntity = UserEntity(id = dbEntity.acceptedUserId)

        return orderLineEntity
    }
}