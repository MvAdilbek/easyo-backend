package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.dataproviders.entity.MenuDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object MenuDbEntityConverter : DbEntityConverter<MenuDbEntity, MenuEntity> {

    override fun toDbEntity(entity: MenuEntity): MenuDbEntity {
        val menuDbEntity = MenuDbEntity()

        menuDbEntity.id = entity.id
        menuDbEntity.menuType = entity.menuType
        menuDbEntity.fromDate = entity.fromDate
        menuDbEntity.toDate = entity.toDate

        return menuDbEntity
    }

    override fun toEntity(dbEntity: MenuDbEntity, isView: Boolean): MenuEntity {
        val menuEntity = MenuEntity()

        menuEntity.id = dbEntity.id
        menuEntity.menuType = dbEntity.menuType
        menuEntity.fromDate = dbEntity.fromDate
        menuEntity.toDate = dbEntity.toDate
        
        return menuEntity
    }
}