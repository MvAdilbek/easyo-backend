package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.dataproviders.entity.SessionDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object SessionDbEntityConverter : DbEntityConverter<SessionDbEntity, SessionEntity> {

    override fun toDbEntity(entity: SessionEntity): SessionDbEntity {
        val sessionDbEntity = SessionDbEntity()

        sessionDbEntity.id = entity.id
        sessionDbEntity.token = entity.token
        sessionDbEntity.socketSessionId = entity.socketSessionId
        sessionDbEntity.userId = entity.userEntity?.id
        sessionDbEntity.entryTime = entity.entryTime

        return sessionDbEntity
    }

    override fun toEntity(dbEntity: SessionDbEntity, isView: Boolean): SessionEntity {
        val sessionEntity = SessionEntity()

        sessionEntity.id = dbEntity.id
        sessionEntity.deleted = dbEntity.deleted
        sessionEntity.token = dbEntity.token
        sessionEntity.socketSessionId = dbEntity.socketSessionId
        sessionEntity.entryTime = dbEntity.entryTime
        sessionEntity.userEntity = UserEntity(id = dbEntity.userId)

        return sessionEntity
    }
}