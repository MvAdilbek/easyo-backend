package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.core.enums.ResourceTypeEnum
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import javax.persistence.*

@Entity
@QueryEntity
@Table(name = "resources")
@Loader(namedQuery = "findResourcesDbById")
@NamedQuery(name = "findResourcesDbById",
            query = "SELECT ent FROM ResourceDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE resources SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class ResourceDbEntity : BaseDbEntity() {
    @Column(name = "resource_url")
    var resourceUrl: String? = null

    @Enumerated(EnumType.STRING)
    @Column(name = "resource_type")
    var resourceTypeEnum: ResourceTypeEnum? = null
}