package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.MealEntity
import kz.easy_o.dataproviders.entity.MealDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object MealDbEntityConverter : DbEntityConverter<MealDbEntity, MealEntity> {

    override fun toDbEntity(entity: MealEntity): MealDbEntity {
        val mealDbEntity = MealDbEntity()

        mealDbEntity.id = entity.id
        mealDbEntity.name = entity.name
        mealDbEntity.description = entity.description
        mealDbEntity.unitPrice = entity.unitPrice
        mealDbEntity.size = entity.size
        mealDbEntity.imageUrl = entity.imageUrl

        return mealDbEntity
    }

    override fun toEntity(dbEntity: MealDbEntity, isView: Boolean): MealEntity {
        val mealEntity = MealEntity()

        mealEntity.id = dbEntity.id
        mealEntity.name = dbEntity.name
        mealEntity.description = dbEntity.description
        mealEntity.unitPrice = dbEntity.unitPrice
        mealEntity.size = dbEntity.size
        mealEntity.imageUrl = dbEntity.imageUrl

        return mealEntity
    }
}