package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.NamedQuery
import javax.persistence.Table

@Entity
@QueryEntity
@Table(name = "sessions")
@Loader(namedQuery = "findSessionDbById")
@NamedQuery(name = "findSessionDbById",
            query = "SELECT ent FROM SessionDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE sessions SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class SessionDbEntity : BaseDbEntity() {
    @Column(name = "token")
    var token: String? = null

    @Column(name = "socket_session_id")
    var socketSessionId: String? = null

    @Column(name = "user_id")
    var userId: Long? = null

    @Column(name = "entry_time")
    var entryTime: LocalDateTime? = null
}