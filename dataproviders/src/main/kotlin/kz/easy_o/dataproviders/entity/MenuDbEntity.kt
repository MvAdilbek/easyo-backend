package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.NamedQuery
import javax.persistence.Table

@Entity
@QueryEntity
@Table(name = "menus")
@Loader(namedQuery = "findMenusDbById")
@NamedQuery(name = "findMenusDbById",
            query = "SELECT ent FROM MenuDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE menus SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class MenuDbEntity : BaseDbEntity() {
    @Column(name = "menu_type")
    var menuType: String? = null

    @Column(name = "from_date")
    var fromDate: LocalDate? = null

    @Column(name = "to_date")
    var toDate: LocalDate? = null
}