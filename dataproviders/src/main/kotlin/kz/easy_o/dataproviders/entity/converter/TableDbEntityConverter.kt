package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.TableEntity
import kz.easy_o.dataproviders.entity.TableDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object TableDbEntityConverter : DbEntityConverter<TableDbEntity, TableEntity> {

    override fun toDbEntity(entity: TableEntity): TableDbEntity {
        val tableDbEntity = TableDbEntity()

        tableDbEntity.id = entity.id
        tableDbEntity.size = entity.size
        tableDbEntity.reserved = entity.reserved
        tableDbEntity.number = entity.number

        return tableDbEntity
    }

    override fun toEntity(dbEntity: TableDbEntity, isView: Boolean): TableEntity {
        val tableEntity = TableEntity()

        tableEntity.id = dbEntity.id
        tableEntity.deleted = dbEntity.deleted
        tableEntity.size = dbEntity.size
        tableEntity.reserved = dbEntity.reserved
        tableEntity.number = dbEntity.number

        return tableEntity
    }
}