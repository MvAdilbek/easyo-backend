package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.NamedQuery
import javax.persistence.Table

@Entity
@QueryEntity
@Table(name = "tables")
@Loader(namedQuery = "findTableDbById")
@NamedQuery(name = "findTableDbById",
            query = "SELECT ent FROM TableDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE tables SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class TableDbEntity : BaseDbEntity() {
    @Column(name = "size")
    var size: Int? = null

    @Column(name = "reserved")
    var reserved: Boolean? = null

    @Column(name = "number")
    var number: String? = null
}