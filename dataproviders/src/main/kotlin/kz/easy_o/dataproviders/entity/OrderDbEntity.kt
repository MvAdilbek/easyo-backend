package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.NamedQuery
import javax.persistence.Table

@Entity
@QueryEntity
@Table(name = "orders")
@Loader(namedQuery = "findOrderDbById")
@NamedQuery(name = "findOrderDbById",
            query = "SELECT ent FROM OrderDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE orders SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class OrderDbEntity : BaseDbEntity() {
    @Column(name = "ordered_date")
    var orderedDate: LocalDateTime? = null

    @Column(name = "table_id")
    var tableId: Long? = null

    @Column(name = "accepted_user_id")
    var acceptedUserId: Long? = null

    @Column(name = "comment")
    var comment: String? = null
}