package kz.easy_o.dataproviders.entity

import com.querydsl.core.annotations.QueryEntity
import kz.easy_o.core.enums.UserPositionEnum
import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.hibernate.annotations.Loader
import org.hibernate.annotations.ResultCheckStyle
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import javax.persistence.*

@Entity
@QueryEntity
@Table(name = "users")
@Loader(namedQuery = "findUserDbById")
@NamedQuery(name = "findUserDbById",
            query = "SELECT ent FROM UserDbEntity ent WHERE ent.deleted = 0 AND ent.id = ?1")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE users SET deleted = (EXTRACT(EPOCH FROM (SELECT NOW())) * 1000) WHERE id = ?",
           check = ResultCheckStyle.COUNT)
class UserDbEntity : BaseDbEntity() {
    @Column(name = "image_url")
    var imageUrl: String? = null

    @Column(name = "first_name")
    var firstName: String? = null

    @Column(name = "last_name")
    var lastName: String? = null

    @Column(name = "email")
    var email: String? = null

    @Column(name = "password")
    var password: String? = null

    @Column(name = "phone_number")
    var phoneNumber: String? = null

    @Enumerated(EnumType.STRING)
    @Column(name = "user_position")
    var userPositionEnum: UserPositionEnum? = null
}