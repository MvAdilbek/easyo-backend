package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.UserEntity
import kz.easy_o.dataproviders.entity.UserDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object UserDbEntityConverter : DbEntityConverter<UserDbEntity, UserEntity> {

    override fun toDbEntity(entity: UserEntity): UserDbEntity {
        val userDbEntity = UserDbEntity()

        userDbEntity.id = entity.id
        userDbEntity.imageUrl = entity.imageUrl
        userDbEntity.firstName = entity.firstName
        userDbEntity.lastName = entity.lastName
        userDbEntity.email = entity.email
        userDbEntity.password = entity.password
        userDbEntity.phoneNumber = entity.phoneNumber
        userDbEntity.userPositionEnum = entity.userPositionEnum

        return userDbEntity
    }

    override fun toEntity(dbEntity: UserDbEntity, isView: Boolean): UserEntity {
        val userEntity = UserEntity()

        userEntity.id = dbEntity.id
        userEntity.deleted = dbEntity.deleted
        userEntity.imageUrl = dbEntity.imageUrl
        userEntity.firstName = dbEntity.firstName
        userEntity.lastName = dbEntity.lastName
        userEntity.email = dbEntity.email
        userEntity.password = dbEntity.password
        userEntity.phoneNumber = dbEntity.phoneNumber
        userEntity.userPositionEnum = dbEntity.userPositionEnum

        return userEntity
    }
}