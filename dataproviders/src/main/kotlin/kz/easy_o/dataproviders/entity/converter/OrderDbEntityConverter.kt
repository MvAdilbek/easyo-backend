package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.entity.TableEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.dataproviders.entity.OrderDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object OrderDbEntityConverter : DbEntityConverter<OrderDbEntity, OrderEntity> {

    override fun toDbEntity(entity: OrderEntity): OrderDbEntity {
        val orderDbEntity = OrderDbEntity()

        orderDbEntity.id = entity.id
        orderDbEntity.orderedDate = entity.orderedDate
        orderDbEntity.tableId = entity.tableEntity?.id
        orderDbEntity.acceptedUserId = entity.acceptedUserEntity?.id
        orderDbEntity.comment = entity.comment

        return orderDbEntity
    }

    override fun toEntity(dbEntity: OrderDbEntity, isView: Boolean): OrderEntity {
        val orderEntity = OrderEntity()

        orderEntity.id = dbEntity.id
        orderEntity.deleted = dbEntity.deleted
        orderEntity.orderedDate = dbEntity.orderedDate
        orderEntity.tableEntity = TableEntity(id = dbEntity.tableId)
        orderEntity.acceptedUserEntity = UserEntity(id = dbEntity.acceptedUserId)
        orderEntity.comment = dbEntity.comment

        return orderEntity
    }
}