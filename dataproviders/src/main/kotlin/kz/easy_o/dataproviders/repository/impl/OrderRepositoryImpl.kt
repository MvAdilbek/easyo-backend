package kz.easy_o.dataproviders.repository.impl

import com.querydsl.core.group.GroupBy
import com.querydsl.core.types.Projections
import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.parameter.OrderParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QMealDbEntity
import kz.easy_o.dataproviders.entity.QOrderDbEntity
import kz.easy_o.dataproviders.entity.QOrderLineDbEntity
import kz.easy_o.dataproviders.entity.converter.MealDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.OrderDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.OrderLineDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaOrderDbRepository
import kz.easy_o.dataproviders.repository.JpaOrderLineDbRepository
import kz.easy_o.usecases.gateway.OrderRepository
import org.springframework.transaction.annotation.Transactional

open class OrderRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaOrderDbRepository: JpaOrderDbRepository,
        private val jpaOrderLineDbRepository: JpaOrderLineDbRepository
) : OrderRepository {

    private val qOrderDbEntity = QOrderDbEntity.orderDbEntity
    private val qOrderLineDbEntity = QOrderLineDbEntity.orderLineDbEntity
    private val qMealDbEntity = QMealDbEntity.mealDbEntity

    @Transactional
    override fun findPageByParam(orderParam: OrderParam): Page<OrderEntity> {
        val data = getJpaQueryByParam(orderParam)
                .select(qOrderDbEntity).page(orderParam)
                .fetchMap(OrderDbEntityConverter::toEntity)
        val total = getJpaQueryByParam(orderParam)
                .fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getJpaQueryByParam(orderParam: OrderParam): JPAQuery<*> {
        val query = jpaQueryFactory.from(qOrderDbEntity)

        if (orderParam.acceptedUserId != null)
            query.where(qOrderDbEntity.acceptedUserId.eq(orderParam.acceptedUserId))

        return query
    }

    @Transactional
    override fun findById(id: Long): OrderEntity? {
        val query = jpaQueryFactory.from(qOrderDbEntity)
                .leftJoin(qOrderLineDbEntity).on(qOrderLineDbEntity.orderId.eq(qOrderDbEntity.id))
                .leftJoin(qMealDbEntity).on(qOrderLineDbEntity.mealId.eq(qMealDbEntity.id))
                .where(qOrderDbEntity.id.eq(id))

        val projection = Projections.tuple(qOrderLineDbEntity, qMealDbEntity)

        val transform = query.transform(GroupBy.groupBy(qOrderDbEntity.id)
                                                .`as`(qOrderDbEntity,
                                                      GroupBy.list(projection)))

        return transform.map {
            val group = it.value
            val orderEntity = OrderDbEntityConverter.toEntity(group.getOne(qOrderDbEntity))
            orderEntity.orderLineEntityList =
                    group.getList(projection).mapNotNull { p ->
                        val orderLineEntity = OrderLineDbEntityConverter.toEntityOptional(p.get(qOrderLineDbEntity))
                        orderLineEntity?.mealEntity = MealDbEntityConverter.toEntityOptional(p.get(qMealDbEntity))
                        orderLineEntity
                    }
            orderEntity
        }.firstOrNull()
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qOrderDbEntity)
                .set(qOrderDbEntity.deleted, System.nanoTime())
                .where(qOrderDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(orderEntity: OrderEntity): OrderEntity {
        if (orderEntity.isNewEntity()) {
            val orderDbEntity = OrderDbEntityConverter.toDbEntity(orderEntity)
            val orderId = jpaOrderDbRepository.save(orderDbEntity).id!!
            if (orderEntity.orderLineEntityList?.isEmpty() == false) {
                val orderLineDbEntityList =
                        orderEntity.orderLineEntityList?.map {
                            it.orderEntity = OrderEntity(id = orderId)
                            OrderLineDbEntityConverter.toDbEntity(it)
                        }!!
                jpaOrderLineDbRepository.saveAll(orderLineDbEntityList)
            }
            return findById(orderId)!!
        }
        jpaQueryFactory.update(qOrderDbEntity)
                .set(qOrderDbEntity.orderedDate, orderEntity.orderedDate)
                .set(qOrderDbEntity.tableId, orderEntity.tableEntity?.id)
                .set(qOrderDbEntity.acceptedUserId, orderEntity.acceptedUserEntity?.id)
                .where(qOrderDbEntity.id.eq(orderEntity.id))
                .execute()
        return findById(orderEntity.id!!)!!
    }
}