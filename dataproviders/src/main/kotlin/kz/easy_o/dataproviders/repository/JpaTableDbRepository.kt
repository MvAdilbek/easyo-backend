package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.TableDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaTableDbRepository : BaseJpaRepository<TableDbEntity>