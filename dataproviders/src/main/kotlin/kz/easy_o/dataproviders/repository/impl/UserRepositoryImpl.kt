package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.core.parameter.UserParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QUserDbEntity
import kz.easy_o.dataproviders.entity.UserDbEntity
import kz.easy_o.dataproviders.entity.converter.UserDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaUserDbRepository
import kz.easy_o.usecases.gateway.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional

open class UserRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaUserDbRepository: JpaUserDbRepository
) : UserRepository {

    private val qUserDbEntity = QUserDbEntity.userDbEntity

    @Transactional
    override fun findPageByParam(userParam: UserParam): Page<UserEntity> {
        val data = getQueryByParam(userParam)
                .page(userParam)
                .fetchMap { UserDbEntityConverter.toEntity(it, true) }
        val total = getQueryByParam(userParam).fetchCount()
        return Page(data = data,
                    total = total)
    }

    @Transactional
    override fun findByEmailAndPassword(email: String, password: String): UserEntity? {
        return jpaQueryFactory.selectFrom(qUserDbEntity)
                .where(qUserDbEntity.email.eq(email),
                       qUserDbEntity.password.eq(password))
                .fetchOne()
                ?.let(UserDbEntityConverter::toEntity)
    }

    private fun getQueryByParam(userParam: UserParam): JPAQuery<UserDbEntity> {
        val query = jpaQueryFactory.selectFrom(qUserDbEntity)
        if (userParam.phoneNumber != null) {
            query.where(qUserDbEntity.phoneNumber.eq(userParam.phoneNumber))
        }
        return query
    }

    @Transactional
    override fun findById(id: Long): UserEntity? {
        val userDbEntity = jpaUserDbRepository.findByIdOrNull(id)
        return UserDbEntityConverter.toEntityOptional(userDbEntity)
    }

    @Transactional
    override fun save(userEntity: UserEntity): UserEntity {
        if (!userEntity.isNewEntity()) {
            jpaQueryFactory.update(qUserDbEntity)
                    .set(qUserDbEntity.imageUrl, userEntity.imageUrl)
                    .set(qUserDbEntity.firstName, userEntity.firstName)
                    .set(qUserDbEntity.lastName, userEntity.lastName)
                    .set(qUserDbEntity.email, userEntity.email)
                    .set(qUserDbEntity.phoneNumber, userEntity.phoneNumber)
                    .where(qUserDbEntity.id.eq(userEntity.id))
                    .execute()

            return userEntity
        }
        val userDbEntity = UserDbEntityConverter.toDbEntity(userEntity)
        val savedUserDbEntity = jpaUserDbRepository.save(userDbEntity)
        return UserDbEntityConverter.toEntity(savedUserDbEntity)
    }

    @Transactional
    override fun updatePassword(userId: Long, hashPassword: String) {
        jpaQueryFactory.update(qUserDbEntity)
                .set(qUserDbEntity.password, hashPassword)
                .where(qUserDbEntity.id.eq(userId))
                .execute()
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaUserDbRepository.deleteById(id)
    }
}