package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.OrderEntity
import kz.easy_o.core.entity.TableEntity
import kz.easy_o.core.parameter.TableParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QBillDbEntity
import kz.easy_o.dataproviders.entity.QOrderDbEntity
import kz.easy_o.dataproviders.entity.QTableDbEntity
import kz.easy_o.dataproviders.entity.converter.OrderDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.TableDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaTableDbRepository
import kz.easy_o.usecases.gateway.TableRepository
import org.springframework.transaction.annotation.Transactional

open class TableRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaTableDbRepository: JpaTableDbRepository
) : TableRepository {

    private val qTableDbEntity = QTableDbEntity.tableDbEntity
    private val qOrderDbEntity = QOrderDbEntity.orderDbEntity
    private val qBillDbEntity = QBillDbEntity.billDbEntity

    @Transactional
    override fun findPageByParam(tableParam: TableParam): Page<TableEntity> {
        val data = fetchDataByParam(tableParam)
        val total = getJpaQueryByParam(tableParam)
                .select(qTableDbEntity.id).fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun fetchDataByParam(tableParam: TableParam): List<TableEntity> {
        val query = getJpaQueryByParam(tableParam)
                .select(qTableDbEntity)
                .page(tableParam)
                .orderBy(qTableDbEntity.number.asc())

        val tables = query.fetchMap(TableDbEntityConverter::toEntity)
        val tableIds = tables.map { it.id!! }
        val orders = fetchActiveOrders(tableIds)

        tables.forEach {
            it.activeOrderEntity = orders.firstOrNull { o -> o.tableEntity?.id!! == it.id }
        }

        return tables
    }

    private fun fetchActiveOrders(tableIds: List<Long>): List<OrderEntity> {
        val query = jpaQueryFactory.from(qOrderDbEntity)
                .leftJoin(qBillDbEntity).on(qBillDbEntity.orderId.eq(qOrderDbEntity.id))
                .where(qOrderDbEntity.tableId.`in`(tableIds),
                       qBillDbEntity.id.isNull)
        return query.select(qOrderDbEntity)
                .fetchMap(OrderDbEntityConverter::toEntity)
    }

    private fun getJpaQueryByParam(tableParam: TableParam): JPAQuery<*> {
        val query = jpaQueryFactory.from(qTableDbEntity)

        if (tableParam.number != null)
            query.where(qTableDbEntity.number.eq(tableParam.number))

        return query
    }

    @Transactional
    override fun findById(id: Long): TableEntity? {
        return jpaQueryFactory.selectFrom(qTableDbEntity)
                .where(qTableDbEntity.id.eq(id))
                .fetchOne()
                ?.let(TableDbEntityConverter::toEntity)
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qTableDbEntity)
                .set(qTableDbEntity.deleted, System.nanoTime())
                .where(qTableDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(tableEntity: TableEntity): Long {
        val tableDbEntity = TableDbEntityConverter.toDbEntity(tableEntity)
        return jpaTableDbRepository.save(tableDbEntity).id!!
    }
}