package kz.easy_o.dataproviders.repository.impl

import com.querydsl.core.group.GroupBy
import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.core.parameter.MenuParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QMealDbEntity
import kz.easy_o.dataproviders.entity.QMenuDbEntity
import kz.easy_o.dataproviders.entity.QMenuMealDbEntity
import kz.easy_o.dataproviders.entity.converter.MealDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.MenuDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaMenuDbRepository
import kz.easy_o.usecases.gateway.MenuRepository
import org.springframework.transaction.annotation.Transactional

open class MenuRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaMenuDbRepository: JpaMenuDbRepository
) : MenuRepository {

    private val qMenuDbEntity = QMenuDbEntity.menuDbEntity
    private val qMenuMealDbEntity = QMenuMealDbEntity.menuMealDbEntity
    private val qMealDbEntity = QMealDbEntity.mealDbEntity

    @Transactional
    override fun findPageByParam(menuParam: MenuParam): Page<MenuEntity> {
        val data = fetchMenuList(menuParam)
        val total = getJpaQueryByParam(menuParam)
                .select(qMenuDbEntity.id)
                .fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun fetchMenuList(menuParam: MenuParam): List<MenuEntity> {
        val query  = getJpaQueryByParam(menuParam)
                .leftJoin(qMenuMealDbEntity).on(qMenuMealDbEntity.menuId.eq(qMenuDbEntity.id))
                .leftJoin(qMealDbEntity).on(qMenuMealDbEntity.mealId.eq(qMealDbEntity.id))
                .page(menuParam)
                .orderBy(qMenuDbEntity.id.asc())

        val transform = query.transform(GroupBy.groupBy(qMenuDbEntity)
                                .`as`(qMenuDbEntity,
                                GroupBy.list(qMealDbEntity)))
        return transform.map {
            val group = it.value
            val menuEntity = MenuDbEntityConverter.toEntity(group.getOne(qMenuDbEntity))
            menuEntity.mealEntityList =
                    group.getList(qMealDbEntity).map(MealDbEntityConverter::toEntity)
            menuEntity
        }
    }

    private fun getJpaQueryByParam(menuParam: MenuParam): JPAQuery<*> {
        val query = jpaQueryFactory.from(qMenuDbEntity)

        if (menuParam.type != null)
            query.where(qMenuDbEntity.menuType.likeIgnoreCase("%${menuParam.type}%"))

        return query
    }

    @Transactional
    override fun findById(id: Long): MenuEntity? {
        return jpaQueryFactory.selectFrom(qMenuDbEntity)
                .where(qMenuDbEntity.id.eq(id))
                .fetchOne()
                ?.let(MenuDbEntityConverter::toEntity)
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qMenuDbEntity)
                .set(qMenuDbEntity.deleted, System.nanoTime())
                .where(qMenuDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(menuEntity: MenuEntity): MenuEntity {
        val menuDbEntity = MenuDbEntityConverter.toDbEntity(menuEntity)
        return jpaMenuDbRepository.save(menuDbEntity).let(MenuDbEntityConverter::toEntity)
    }
}