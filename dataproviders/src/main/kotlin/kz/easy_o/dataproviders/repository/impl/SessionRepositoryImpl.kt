package kz.easy_o.dataproviders.repository.impl

import com.querydsl.core.Tuple
import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.parameter.SessionParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QSessionDbEntity
import kz.easy_o.dataproviders.entity.QUserDbEntity
import kz.easy_o.dataproviders.entity.converter.SessionDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.UserDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaSessionDbRepository
import kz.easy_o.usecases.gateway.SessionRepository
import org.springframework.transaction.annotation.Transactional

open class SessionRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaSessionDbRepository: JpaSessionDbRepository
) : SessionRepository {

    private val qSessionDbEntity = QSessionDbEntity.sessionDbEntity
    private val qUserDbEntity = QUserDbEntity.userDbEntity

    @Transactional
    override fun pageByParam(sessionParam: SessionParam): Page<SessionEntity> {
        val data = getQueryByParam(sessionParam)
                .page(sessionParam)
                .fetchMap {
                    val sessionEntity = SessionDbEntityConverter.toEntity(it.get(qSessionDbEntity)!!)
                    sessionEntity.userEntity = UserDbEntityConverter.toEntity(it.get(qUserDbEntity)!!)
                    sessionEntity
                }
        val total = getQueryByParam(sessionParam).fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getQueryByParam(sessionParam: SessionParam): JPAQuery<Tuple> {
        return jpaQueryFactory.from(qSessionDbEntity)
                .innerJoin(qUserDbEntity).on(qSessionDbEntity.userId.eq(qUserDbEntity.id))
                .select(qSessionDbEntity,
                        qUserDbEntity)
    }

    @Transactional
    override fun findById(id: Long): SessionEntity? {
        return jpaQueryFactory.from(qSessionDbEntity)
                .innerJoin(qUserDbEntity).on(qSessionDbEntity.userId.eq(qUserDbEntity.id))
                .where(qSessionDbEntity.id.eq(id))
                .select(qSessionDbEntity,
                        qUserDbEntity)
                .fetchOne()
                ?.let {
                    val sessionEntity = SessionDbEntityConverter.toEntity(it.get(qSessionDbEntity)!!)
                    sessionEntity.userEntity = UserDbEntityConverter.toEntity(it.get(qUserDbEntity)!!)
                    sessionEntity
                }
    }

    @Transactional
    override fun findByToken(token: String): SessionEntity? {
        return jpaQueryFactory.selectFrom(qSessionDbEntity)
                .where(qSessionDbEntity.token.eq(token))
                .fetchOne().let(SessionDbEntityConverter::toEntityOptional)
    }

    @Transactional
    override fun deleteByUserId(userId: Long) {
        jpaQueryFactory.update(qSessionDbEntity)
                .set(qSessionDbEntity.deleted, System.nanoTime())
                .where(qSessionDbEntity.userId.eq(userId))
                .execute()
    }

    @Transactional
    override fun save(sessionEntity: SessionEntity): Long {
        val sessionDbEntity = SessionDbEntityConverter.toDbEntity(sessionEntity)
        return jpaSessionDbRepository.save(sessionDbEntity).id!!
    }

    @Transactional
    override fun updateSocketSessionId(sessionId: Long, socketSessionId: String) {
        jpaQueryFactory.update(qSessionDbEntity)
                .set(qSessionDbEntity.socketSessionId, socketSessionId)
                .where(qSessionDbEntity.id.eq(sessionId))
                .execute()
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qSessionDbEntity)
                .set(qSessionDbEntity.deleted, System.nanoTime())
                .where(qSessionDbEntity.id.eq(id))
                .execute()
    }
}