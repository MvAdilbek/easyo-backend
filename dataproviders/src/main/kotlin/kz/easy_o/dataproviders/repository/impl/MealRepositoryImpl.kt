package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.MealEntity
import kz.easy_o.core.entity.MenuEntity
import kz.easy_o.core.parameter.MealParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.MenuMealDbEntity
import kz.easy_o.dataproviders.entity.QMealDbEntity
import kz.easy_o.dataproviders.entity.QMenuDbEntity
import kz.easy_o.dataproviders.entity.QMenuMealDbEntity
import kz.easy_o.dataproviders.entity.converter.MealDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaMealDbRepository
import kz.easy_o.dataproviders.repository.JpaMenuMealDbRepository
import kz.easy_o.usecases.gateway.MealRepository
import org.springframework.transaction.annotation.Transactional

open class MealRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaMenuDbRepository: JpaMealDbRepository,
        private val jpaMenuMealDbRepository: JpaMenuMealDbRepository
) : MealRepository {

    private val qMenuDbEntity = QMenuDbEntity.menuDbEntity
    private val qMealDbEntity = QMealDbEntity.mealDbEntity
    private val qMenuMealDbEntity = QMenuMealDbEntity.menuMealDbEntity

    @Transactional
    override fun findPageByParam(mealParam: MealParam): Page<MealEntity> {
        val data = getJpaQueryByParam(mealParam)
                .select(qMealDbEntity,
                        qMenuMealDbEntity.menuId).page(mealParam)
                .orderBy(qMenuDbEntity.id.asc())
                .fetchMap {
                    val mealEntity = MealDbEntityConverter.toEntity(it.get(qMealDbEntity)!!)
                    mealEntity.menuEntity = MenuEntity(id = it.get(qMenuMealDbEntity.menuId))
                    mealEntity
                }
        val total = getJpaQueryByParam(mealParam)
                .select(qMealDbEntity.id)
                .fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getJpaQueryByParam(mealParam: MealParam): JPAQuery<out Any> {
        val query = jpaQueryFactory.from(qMealDbEntity)
                .innerJoin(qMenuMealDbEntity).on(qMenuMealDbEntity.mealId.eq(qMealDbEntity.id))
                .innerJoin(qMenuDbEntity).on(qMenuMealDbEntity.menuId.eq(qMenuDbEntity.id))

        if (mealParam.name != null)
            query.where(qMealDbEntity.name.likeIgnoreCase("%${mealParam.name}%"))
        if (mealParam.menuId != null)
            query.where(qMenuDbEntity.id.eq(mealParam.menuId))

        return query
    }

    @Transactional
    override fun findById(id: Long): MealEntity? {
        return jpaQueryFactory.from(qMealDbEntity)
                .innerJoin(qMenuMealDbEntity).on(qMenuMealDbEntity.mealId.eq(qMealDbEntity.id))
                .where(qMealDbEntity.id.eq(id))
                .select(qMealDbEntity,
                        qMenuMealDbEntity.menuId)
                .fetchOne()
                ?.let {
                    val mealEntity = MealDbEntityConverter.toEntity(it.get(qMealDbEntity)!!)
                    mealEntity.menuEntity = MenuEntity(id = it.get(qMenuMealDbEntity.menuId))
                    mealEntity
                }
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qMealDbEntity)
                .set(qMealDbEntity.deleted, System.nanoTime())
                .where(qMealDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(mealEntity: MealEntity): MealEntity {
        if (mealEntity.isNewEntity()) {
            val mealDbEntity = MealDbEntityConverter.toDbEntity(mealEntity)
            val savedMealDbEntity = jpaMenuDbRepository.save(mealDbEntity)
            val menuMealDbEntity = MenuMealDbEntity()
            menuMealDbEntity.mealId = savedMealDbEntity.id
            menuMealDbEntity.menuId = mealEntity.menuEntity?.id
            jpaMenuMealDbRepository.save(menuMealDbEntity)
            return findById(savedMealDbEntity.id!!)!!
        }
        else {
            jpaQueryFactory.update(qMealDbEntity)
                    .set(qMealDbEntity.name, mealEntity.name)
                    .set(qMealDbEntity.description, mealEntity.description)
                    .set(qMealDbEntity.unitPrice, mealEntity.unitPrice)
                    .set(qMealDbEntity.size, mealEntity.size)
                    .set(qMealDbEntity.imageUrl, mealEntity.imageUrl)
                    .where(qMealDbEntity.id.eq(mealEntity.id))
                    .execute()
            return findById(mealEntity.id!!)!!
        }
    }
}