package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.OrderLineDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaOrderLineDbRepository : BaseJpaRepository<OrderLineDbEntity>