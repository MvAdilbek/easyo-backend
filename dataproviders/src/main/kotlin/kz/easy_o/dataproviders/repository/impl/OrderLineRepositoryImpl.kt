package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.OrderLineEntity
import kz.easy_o.core.parameter.OrderLineParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.entity.OrderLineDbEntity
import kz.easy_o.dataproviders.entity.QOrderLineDbEntity
import kz.easy_o.dataproviders.entity.converter.OrderLineDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaOrderLineDbRepository
import kz.easy_o.usecases.gateway.OrderLineRepository
import org.springframework.transaction.annotation.Transactional

open class OrderLineRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaOrderLineDbRepository: JpaOrderLineDbRepository
) : OrderLineRepository {

    private val qOrderLineDbEntity = QOrderLineDbEntity.orderLineDbEntity

    @Transactional
    override fun findPageByParam(orderLineParam: OrderLineParam): Page<OrderLineEntity> {
        val data = getJpaQueryByParam(orderLineParam)
                .fetchMap(OrderLineDbEntityConverter::toEntity)
        val total = getJpaQueryByParam(orderLineParam)
                .fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getJpaQueryByParam(orderLineParam: OrderLineParam): JPAQuery<OrderLineDbEntity> {
        val query = jpaQueryFactory.from(qOrderLineDbEntity)
                .select(qOrderLineDbEntity)

        if (orderLineParam.orderId != null)
            query.where(qOrderLineDbEntity.orderId.eq(orderLineParam.orderId))
        if (orderLineParam.orderLineStatusEnum != null)
            query.where(qOrderLineDbEntity.orderLineStatusEnum.`in`(orderLineParam.orderLineStatusEnum))

        return query
    }

    @Transactional
    override fun findById(id: Long): OrderLineEntity? {
        return jpaQueryFactory.selectFrom(qOrderLineDbEntity)
                .where(qOrderLineDbEntity.id.eq(id))
                .fetchOne()
                ?.let(OrderLineDbEntityConverter::toEntity)
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qOrderLineDbEntity)
                .set(qOrderLineDbEntity.deleted, System.nanoTime())
                .where(qOrderLineDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(orderLineEntity: OrderLineEntity): Long {
        val orderLineDbEntity = OrderLineDbEntityConverter.toDbEntity(orderLineEntity)
        return jpaOrderLineDbRepository.save(orderLineDbEntity).id!!
    }

}