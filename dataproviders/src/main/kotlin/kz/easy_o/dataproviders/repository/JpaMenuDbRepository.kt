package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.MenuDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaMenuDbRepository : BaseJpaRepository<MenuDbEntity>