package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.BillEntity
import kz.easy_o.core.parameter.BillParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QBillDbEntity
import kz.easy_o.dataproviders.entity.QOrderDbEntity
import kz.easy_o.dataproviders.entity.QTableDbEntity
import kz.easy_o.dataproviders.entity.converter.BillDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.OrderDbEntityConverter
import kz.easy_o.dataproviders.entity.converter.TableDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaBillDbRepository
import kz.easy_o.usecases.gateway.BillRepository
import org.springframework.transaction.annotation.Transactional

open class BillRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaBillDbRepository: JpaBillDbRepository
) : BillRepository {

    private val qBillDbEntity = QBillDbEntity.billDbEntity
    private val qOrderDbEntity = QOrderDbEntity.orderDbEntity
    private val qTableDbEntity = QTableDbEntity.tableDbEntity

    @Transactional
    override fun findPageByParam(billParam: BillParam): Page<BillEntity> {
        val data = getJpaQueryParam(billParam)
                .select(qBillDbEntity).page(billParam)
                .orderBy(qBillDbEntity.id.asc())
                .fetchMap(BillDbEntityConverter::toEntity)
        val total = getJpaQueryParam(billParam)
                .fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getJpaQueryParam(billParam: BillParam): JPAQuery<*> {
        val query = jpaQueryFactory.from(qBillDbEntity)

        if (billParam.orderId != null)
            query.where(qBillDbEntity.orderId.eq(billParam.orderId))
        if (billParam.tableId != null)
            query.innerJoin(qOrderDbEntity).on(qBillDbEntity.orderId.eq(qOrderDbEntity.id))
                    .where(qOrderDbEntity.tableId.eq(billParam.tableId))

        return query
    }

    @Transactional
    override fun findById(id: Long): BillEntity? {
        return jpaQueryFactory.from(qBillDbEntity)
                .innerJoin(qOrderDbEntity).on(qBillDbEntity.orderId.eq(qOrderDbEntity.id))
                .innerJoin(qTableDbEntity).on(qOrderDbEntity.tableId.eq(qTableDbEntity.id))
                .where(qBillDbEntity.id.eq(id))
                .select(qBillDbEntity,
                        qOrderDbEntity,
                        qTableDbEntity)
                .fetchOne()
                ?.let {
                    val billEntity =
                            BillDbEntityConverter.toEntity(it.get(qBillDbEntity)!!)
                    billEntity.orderEntity =
                            OrderDbEntityConverter.toEntity(it.get(qOrderDbEntity)!!)
                    billEntity.orderEntity?.tableEntity =
                            TableDbEntityConverter.toEntity(it.get(qTableDbEntity)!!)
                    billEntity
                }
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaQueryFactory.update(qBillDbEntity)
                .set(qBillDbEntity.deleted, System.nanoTime())
                .where(qBillDbEntity.id.eq(id))
                .execute()
    }

    @Transactional
    override fun save(billEntity: BillEntity): BillEntity {
        val billDbEntity = BillDbEntityConverter.toDbEntity(billEntity)
        return jpaBillDbRepository.save(billDbEntity).let(BillDbEntityConverter::toEntity)
    }
}