package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.MenuMealDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaMenuMealDbRepository : BaseJpaRepository<MenuMealDbEntity>