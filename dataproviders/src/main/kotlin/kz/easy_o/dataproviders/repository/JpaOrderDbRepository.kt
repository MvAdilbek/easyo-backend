package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.OrderDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaOrderDbRepository : BaseJpaRepository<OrderDbEntity>