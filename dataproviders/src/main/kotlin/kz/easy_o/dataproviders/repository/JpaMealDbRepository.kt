package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.MealDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaMealDbRepository : BaseJpaRepository<MealDbEntity>