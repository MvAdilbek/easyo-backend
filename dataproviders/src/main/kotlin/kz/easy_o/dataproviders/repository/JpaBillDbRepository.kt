package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.BillDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaBillDbRepository : BaseJpaRepository<BillDbEntity>