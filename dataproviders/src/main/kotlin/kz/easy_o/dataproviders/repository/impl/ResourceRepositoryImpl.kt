package kz.easy_o.dataproviders.repository.impl

import com.querydsl.jpa.impl.JPAQuery
import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.core.Page
import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.core.parameter.ResourceParam
import kz.easy_o.dataproviders.common.extension.fetchMap
import kz.easy_o.dataproviders.common.extension.page
import kz.easy_o.dataproviders.entity.QResourceDbEntity
import kz.easy_o.dataproviders.entity.ResourceDbEntity
import kz.easy_o.dataproviders.entity.converter.ResourceDbEntityConverter
import kz.easy_o.dataproviders.repository.JpaResourceDbRepository
import kz.easy_o.usecases.gateway.ResourceRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional

open class ResourceRepositoryImpl(
        private val jpaQueryFactory: JPAQueryFactory,
        private val jpaResourceDbRepository: JpaResourceDbRepository
) : ResourceRepository {

    private val qResourceDbEntity = QResourceDbEntity.resourceDbEntity

    @Transactional
    override fun pageByParam(requestParam: ResourceParam): Page<ResourceEntity> {
        val data = getQueryByParam(requestParam).page(requestParam).fetchMap(ResourceDbEntityConverter::toEntity)
        val total = getQueryByParam(requestParam).fetchCount()
        return Page(data = data,
                    total = total)
    }

    private fun getQueryByParam(requestParam: ResourceParam): JPAQuery<ResourceDbEntity> {
        return jpaQueryFactory.selectFrom(qResourceDbEntity)
    }

    @Transactional
    override fun findById(id: Long): ResourceEntity? {
        return jpaResourceDbRepository.findByIdOrNull(id)
                .let(ResourceDbEntityConverter::toEntityOptional)
    }

    @Transactional
    override fun save(resourceEntity: ResourceEntity): Long {
        val resourceDbEntity = ResourceDbEntityConverter.toDbEntity(resourceEntity)
        return jpaResourceDbRepository.save(resourceDbEntity).id!!
    }

    @Transactional
    override fun deleteById(id: Long) {
        jpaResourceDbRepository.deleteById(id)
    }
}