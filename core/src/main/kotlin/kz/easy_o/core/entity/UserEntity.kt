package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import kz.easy_o.core.enums.UserPositionEnum

class UserEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var imageUrl: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null,
        var password: String? = null,
        var phoneNumber: String? = null,
        var userPositionEnum: UserPositionEnum? = null
) : BaseEntity() {
    override fun toString(): String {
        return "{ id=$id, " +
                "deleted=$deleted, " +
                "imageUrl=$imageUrl, " +
                "firstName=$firstName, " +
                "lastName=$lastName, " +
                "email=$email, " +
                "password=$password, " +
                "userPositionEnum=$userPositionEnum, " +
                "phoneNumber=$phoneNumber }"
    }
}