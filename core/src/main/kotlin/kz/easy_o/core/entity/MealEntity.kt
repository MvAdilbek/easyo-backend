package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import java.math.BigDecimal

class MealEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var name: String? = null,
        var description: String? = null,
        var unitPrice: BigDecimal? = null,
        var size: Int? = null,
        var imageUrl: String? = null,
        var menuEntity: MenuEntity? = null
) : BaseEntity()