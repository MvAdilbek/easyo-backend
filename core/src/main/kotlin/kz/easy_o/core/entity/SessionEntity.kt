package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import java.time.LocalDateTime

class SessionEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var token: String? = null,
        var socketSessionId: String? = null,
        var entryTime: LocalDateTime? = null,
        var userEntity: UserEntity? = null
) : BaseEntity() {
    override fun toString(): String {
        return "{ id=$id, " +
                "deleted=$deleted, " +
                "token=$token, " +
                "socketSessionId=$socketSessionId, " +
                "entryTime=$entryTime," +
                "userEntity=$userEntity }"
    }
}