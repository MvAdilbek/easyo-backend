package kz.easy_o.core.entity.base

open class BaseEntity {
    open var id: Long? = null
    open var deleted: Long? = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseEntity

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

    override fun toString(): String {
        return "{ id=$id, deleted=$deleted }"
    }

    fun isNewEntity(): Boolean {
        return id == null || id == 0L
    }
}