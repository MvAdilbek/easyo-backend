package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import java.time.LocalDateTime

class OrderEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var orderedDate: LocalDateTime? = null,
        var tableEntity: TableEntity? = null,
        var acceptedUserEntity: UserEntity? = null,
        var comment: String? = null,

        var orderLineEntityList: List<OrderLineEntity>? = null
) : BaseEntity() {
    override fun toString(): String {
        return "{ id=$id, " +
                "deleted=$deleted, " +
                "orderedDate=$orderedDate, " +
                "tableId=${tableEntity?.id}, " +
                "acceptedUserId=${acceptedUserEntity?.id} }"
    }
}