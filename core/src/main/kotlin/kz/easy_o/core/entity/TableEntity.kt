package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity

class TableEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var size: Int? = null,
        var reserved: Boolean? = null,
        var number: String? = null,

        var activeOrderEntity: OrderEntity? = null
) : BaseEntity() {
    override fun toString(): String {
        return "{ id=$id, " +
                "deleted=$deleted, " +
                "size=$size, " +
                "reserved=$reserved, " +
                "number=$number, " +
                "activeOrderEntityId=${activeOrderEntity?.id} }"
    }
}