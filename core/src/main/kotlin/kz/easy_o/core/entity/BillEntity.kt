package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import java.math.BigDecimal
import java.time.LocalDateTime

class BillEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var billTime: LocalDateTime? = null,
        var amount: BigDecimal? = null,
        var orderEntity: OrderEntity? = null,
        var acceptedUserEntity: UserEntity? = null
) : BaseEntity()