package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import kz.easy_o.core.enums.OrderLineStatusEnum
import java.math.BigDecimal

class OrderLineEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var quantity: Int? = null,
        var unitPrice: BigDecimal? = null,
        var mealEntity: MealEntity? = null,
        var orderEntity: OrderEntity? = null,
        var status: OrderLineStatusEnum? = null,
        var acceptedUserEntity: UserEntity? = null
) : BaseEntity() {
    override fun toString(): String {
        return "{ id=$id, " +
                "deleted=$deleted, " +
                "quantity=$quantity, " +
                "mealId=${mealEntity?.id}, " +
                "orderId=${orderEntity?.id}, " +
                "status=$status }"
    }
}