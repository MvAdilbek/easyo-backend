package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import java.time.LocalDate

class MenuEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var menuType: String? = null,
        var fromDate: LocalDate? = null,
        var toDate: LocalDate? = null,

        var mealEntityList: List<MealEntity>? = null
) : BaseEntity()