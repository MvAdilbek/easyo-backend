package kz.easy_o.core.entity

import kz.easy_o.core.entity.base.BaseEntity
import kz.easy_o.core.enums.ResourceTypeEnum

class ResourceEntity(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var resourceUrl: String? = null,
        var resourceTypeEnum: ResourceTypeEnum? = null
) : BaseEntity()