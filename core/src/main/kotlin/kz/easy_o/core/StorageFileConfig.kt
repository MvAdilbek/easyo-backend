package kz.easy_o.core

data class StorageFileConfig(
        val path: String
)