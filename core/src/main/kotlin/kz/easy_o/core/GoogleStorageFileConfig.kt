package kz.easy_o.core

data class GoogleStorageFileConfig(
        val mediaLink: String
)