package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class SessionSocketRegisterParam(
        val sessionId: Long,
        val socketSessionId: String
) : Param {
    override fun toString(): String {
        return "{ sessionId=$sessionId, " +
                "socketSessionId=$socketSessionId }"
    }
}