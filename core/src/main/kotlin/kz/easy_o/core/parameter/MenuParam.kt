package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class MenuParam(
        override var page: Int?,
        override var size: Int?,

        val type: String?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size, " +
                "type=$type }"
    }
}