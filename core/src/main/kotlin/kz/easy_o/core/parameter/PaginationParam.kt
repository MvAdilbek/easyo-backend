package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

open class PaginationParam(
        open var page: Int?,
        open var size: Int?
) : Param {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size }"
    }
}