package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class MealParam(
        override var page: Int?,
        override var size: Int?,

        val name: String?,
        val menuId: Long?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size, " +
                "name=$name," +
                "menuId=$menuId }"
    }
}