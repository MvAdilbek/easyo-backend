package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class BillParam(
        override var page: Int?,
        override var size: Int?,

        var tableId: Long?,
        var orderId: Long?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size }"
    }
}