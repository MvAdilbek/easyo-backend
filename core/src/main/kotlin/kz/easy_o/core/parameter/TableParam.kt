package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class TableParam(
        override var page: Int?,
        override var size: Int?,

        val number: String?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size, " +
                "number=$number }"
    }
}