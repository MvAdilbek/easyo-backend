package kz.easy_o.core.parameter

import kz.easy_o.core.enums.OrderLineStatusEnum
import kz.easy_o.core.parameter.intf.Param

data class OrderLineParam(
        override var page: Int?,
        override var size: Int?,

        var orderId: Long?,
        var orderLineStatusEnum: List<OrderLineStatusEnum>?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size, " +
                "orderId=$orderId, " +
                "orderLineStatusEnum=$orderLineStatusEnum }"
    }
}