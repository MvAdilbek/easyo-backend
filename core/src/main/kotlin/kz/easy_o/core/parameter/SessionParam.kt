package kz.easy_o.core.parameter

import kz.easy_o.core.parameter.intf.Param

data class SessionParam(
        override var page: Int?,
        override var size: Int?
) : Param, PaginationParam(page, size) {
    override fun toString(): String {
        return "{ page=$page, " +
                "size=$size }"
    }
}