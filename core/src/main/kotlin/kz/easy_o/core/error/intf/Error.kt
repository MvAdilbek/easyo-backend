package kz.easy_o.core.error.intf

interface Error {
    val message: String
}