package kz.easy_o.core.error

import kz.easy_o.core.enums.ErrorCodeEnum
import kz.easy_o.core.error.intf.Error

open class SimpleError(
        override val message: String,
        val code: ErrorCodeEnum
) : Error