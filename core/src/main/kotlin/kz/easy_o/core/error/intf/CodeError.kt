package kz.easy_o.core.error.intf

import kz.easy_o.core.enums.ErrorCodeEnum

interface CodeError : Error {
    val code: ErrorCodeEnum
}