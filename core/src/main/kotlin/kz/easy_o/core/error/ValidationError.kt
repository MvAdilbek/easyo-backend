package kz.easy_o.core.error

import kz.easy_o.core.enums.ErrorCodeEnum
import kz.easy_o.core.error.intf.CodeError

data class ValidationError(
        override val code: ErrorCodeEnum = ErrorCodeEnum.VALIDATION,
        override val message: String,

        val fields: Collection<FieldError>
) : CodeError