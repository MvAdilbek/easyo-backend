package kz.easy_o.core.enums

enum class OrderLineStatusEnum {
    PREPARE,
    AWAITING,
    DONE
}