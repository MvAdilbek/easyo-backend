package kz.easy_o.core.enums

enum class UserPositionEnum {
    WAITER,
    COOK,
    MANAGER;
}