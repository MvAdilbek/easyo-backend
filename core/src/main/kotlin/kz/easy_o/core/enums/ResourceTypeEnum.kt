package kz.easy_o.core.enums

enum class ResourceTypeEnum {
    IMAGE,
    VIDEO,
    FILE;
}