package kz.easy_o.core.enums

enum class ErrorCodeEnum {
    INTERNAL_SERVER_ERROR,
    NOT_FOUND,
    VALIDATION,
    FORBIDDEN,
    NO_RESOURCE,
}