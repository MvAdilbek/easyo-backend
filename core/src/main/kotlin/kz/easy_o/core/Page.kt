package kz.easy_o.core

data class Page<T>(
        val data: List<T>,
        val total: Long
)